﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataService.InputClass
{
   public class Answer
    {
        public string dsrId { get; set; }
        public string rating { get; set; }
        public string assignmentId { get; set; }
        public string cycleId { get; set; }
        public string questionId { get; set; }
    }
}

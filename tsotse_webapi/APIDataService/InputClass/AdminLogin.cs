﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataService.InputClass
{
     public class AdminLogin
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}

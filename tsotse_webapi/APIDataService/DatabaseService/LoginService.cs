﻿using APIDataService.Constants;
using APIDataService.InputClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataService.DatabaseService
{
    public class LoginService
    {
        private string username;
        private string password;

        public LoginService(string username, string password)
        {
            this.username = username;
            this.password = password;

        }

        string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;

       

        public Tuple<int, string> AdminLogin(AdminLogin _adminLogin)
        {

            Tuple<int, string> tuple = null;

          
            DataBase _con = new DataBase();

            DataSet DS = _con.GetTable("EXEC AdminPanelLogin '" + _adminLogin.userName + "','"+ _adminLogin.password +"'");

            tuple = new Tuple<int, string>(Convert.ToInt32(DS.Tables[0].Rows[0].Field<int>("statuscode")), DS.Tables[0].Rows[0].Field<string>("statusDescription"));

            return tuple;
        }
    }
}

﻿using APIDataService.ExceptionHandler;
using APIDataService.GenericReader;
using APIDataService.InputClass;
using APIDataService.OutputClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataService.DatabaseService
{
    public class QuestionService : CustomException
    {
        private string token;

        public QuestionService(string token)
        {
            this.token = token;
        }

        readonly SqlConnection _sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSOConnection"].ConnectionString);


        public IList<QuestionVM> GetQuestionList()
        {
            IList<QuestionVM> questions = new List<QuestionVM>();
            try
            {
                SqlCommand cmd = new SqlCommand("spGetQuestion", _sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TSOTSECode", token);
                _sqlConnection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                questions = new GenericReader<QuestionVM>().CreateList(rdr);
                rdr.Close();
                _sqlConnection.Close();
            }
            catch (Exception ex)
            {
                if (_sqlConnection.State == ConnectionState.Open)
                {
                    _sqlConnection.Close();
                    LogException(ex);
                }
            }

            return questions;

        }

        public int PostAnswer(Answer answer)
        {
            int responseCode=0;
            try
            {
                SqlCommand cmd = new SqlCommand("spPostAnswer", _sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TSOTSECode", token);
                cmd.Parameters.AddWithValue("@DSRId", answer.dsrId);
                cmd.Parameters.AddWithValue("@rating", answer.rating);
                cmd.Parameters.AddWithValue("@cycleId", answer.cycleId);
                cmd.Parameters.AddWithValue("@assignmentId", answer.assignmentId);
                cmd.Parameters.AddWithValue("@questionId", answer.questionId);

                _sqlConnection.Open();
                responseCode = (int)cmd.ExecuteScalar();
                
                _sqlConnection.Close();
            }
            catch (Exception ex)
            {
                if (_sqlConnection.State == ConnectionState.Open)
                {
                    _sqlConnection.Close();
                    LogException(ex);
                }
            }

            return responseCode;

        }

    }
}

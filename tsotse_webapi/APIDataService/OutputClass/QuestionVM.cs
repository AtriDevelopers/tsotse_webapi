﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDataService.OutputClass
{
    public class QuestionVM
    {
        public int id { get; set; }
        public string title { get; set; }
        public string assignmentId { get; set; }
        public string cycleId { get; set; }
        public string cycleName { get; set; }
        public string categoryName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class MicrosoftUserPojo
    {
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string id { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string displayName { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string giveName { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string jobTitle { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string mail { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string mobilePhone { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string officeLocation { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string preferredLanguage { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string surname { get; set; }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string userPrincipalName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;

namespace TSOWebAPI
{
    public class Utility
    {
        public static string MICROSOFT_GRAPH = "https://graph.microsoft.com/V1.0/maricoapps.onmicrosoft.com/";

        public static bool isTokenValid(String token)
        {

            bool isTokenValid = false;

            //here to implement token validation
            //url to validate: public static final String MICROSOFT_GRAPH = "https://graph.microsoft.com/V1.0/maricoapps.onmicrosoft.com/";
            //This API call will return User Object for which POJO is implemented in class MicrosoftUserPojo.cs

            //for timebeing i am making token as vlaid token


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(MICROSOFT_GRAPH);
            httpWebRequest.Headers["Authorization"] = token;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string responseText = "";
            var encoding = ASCIIEncoding.ASCII;
            using (var reader = new StreamReader(stream: httpResponse.GetResponseStream(), encoding: encoding))
            {
                responseText = reader.ReadToEnd();
            }
            isTokenValid = true;

            return isTokenValid;
        }
    }
}
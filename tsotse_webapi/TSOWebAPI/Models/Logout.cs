﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class Logout
    {
        public string deviceId { get; set; }
        public string tsotoken { get; set; }
    }
}
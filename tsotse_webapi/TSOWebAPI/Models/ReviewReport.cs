﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class ReviewReport
    {
        public string DDName { get; set; }
        public string DSRName { get; set; }
        public string DSRType { get; set; }
        public string Beat { get; set; }
        public string OutletNam { get; set; }
        public string FailedVisitResult { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class PollsSurvey
    {
       // internal DateTime time;

        public string DDCode { get; set; }
        public string DDName { get; set; }
        public string DSRId { get; set; }
        public string DSRName { get; set; }
        public string ActivityName { get; set; }
        public string Status { get; set; }
        public string CompletionDate { get; set; }
        public string Trainlist { get; set; }
    }
    public class Traininglist
    {
        public string Trainlist { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class DsrIncentive1
    {
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string dsrtype { get; set; }
        public string earning_potential { get; set; }
        public string mtd_earning { get; set; }
        public string month { get; set; }
        public string filtermonth { get; set; }
    }
    public class dsrincentivecurrentmonth
    {
        public string months { get; set; }
        public IList<DsrIncentive1> dsrs { get; set; }
    }
    public class dsrwisekpi
    {
        public string kpi { get; set; }
        public string target  { get; set; }
        public string achievement { get; set; }
        public string mtd { get; set; }
        public string slabpercentage { get; set; }
             
    }
      
    public class kpiwisedsr
    {
        public string kpi { get; set; }
        public string DistName { get; set; }
        public string DsrName { get; set; }
        public string kpivalue { get; set; }
        
    }
       
    
}
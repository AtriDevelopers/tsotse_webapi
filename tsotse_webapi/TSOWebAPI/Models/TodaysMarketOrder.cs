﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class TodaysMarketOrder
    {
        public string ddocde { get; set; }
        public string ddname { get; set; }
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string orderdate { get; set; }
        public string ordervalue { get; set; }
    }

    public class TodaysMarketOrderVM : ResponseBase
    {
        public IList<TodaysMarketOrder> data;
    }

}
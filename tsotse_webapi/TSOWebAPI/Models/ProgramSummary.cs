﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class ProgramSummary
    {
        public string Program { get; set; }
        public string NoOfParties { get; set; }
        public string Target { get; set; }
        public string AchievedTarget { get; set; }
        public string NotBilledPhase { get; set; }
        public string NotBilledMonth { get; set; }
    }
}
       
       
      
   
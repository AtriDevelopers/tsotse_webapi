﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class BeatDownloadUploadCount
    {
        public string totaluploadcount { get; set; }
        public string actualuploadcount { get; set; }
        public string totaldownloadcount { get; set; }
        public string actualdownloadcount { get; set; }
        public string todaysdatetime { get; set; }
        
    }
}
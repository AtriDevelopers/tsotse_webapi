﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
  
    public class claimTime
    {
        public string fromdate { get; set; } = System.DateTime.Now.ToString();
        public string todate { get; set; } = System.DateTime.Now.ToString();
        public IList<Dsdsclaim> claimlist { get; set; }
    }
    public class Dsdsclaim
    {

        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string claimamount { get; set; }
        public string claimmonth { get; set; }
        public string balancepending { get; set; }
        public string createddate { get; set; }
        public string claimtype { get; set; }
       
    }
}
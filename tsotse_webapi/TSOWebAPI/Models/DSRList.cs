﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class DSRList
    {
        public string DSRId { get; set; }
        public string DSRName { get; set; }
        public string DistCode { get; set; }
        public string DDName { get; set; }
        public string BloodGroup { get; set; }
        public string DateOfJoining { get; set; }
        public string Experience { get; set; }
        public string image { get; set; }
        public string contact { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class DSR
    {
        public string dsrid { get; set; }
        public string dsrname { get; set; }
    }

    public class DSRVM: ResponseBase
    {
        public List<DSR> dsrlist;
    }
}
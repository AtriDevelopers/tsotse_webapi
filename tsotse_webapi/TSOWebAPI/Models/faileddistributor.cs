﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class faileddistributor
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
    }
    public class PJPAgenda
    {
        public string AgendaTitle { get; set; }
    }

    public class monthdate
    {
        public string date { get; set; }
    }

    public class DistributorVM : ResponseBase
    {
        public List<monthdate> datelist;
        public List<faileddistributor> distributor;
        public List<PJPAgenda> pjpagenda;
    }
}
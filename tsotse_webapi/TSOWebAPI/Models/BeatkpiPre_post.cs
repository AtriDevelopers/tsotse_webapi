﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class BeatkpiPre_post
    {
        //public string ddcode { get; set; }
        public string ddname { get; set; }
       // public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string BeatName { get; set; }
        public string MILEC_Post { get; set; }
        public string MILEC_Pre { get; set; }
        public string BPM_Post { get; set; }
        public string BPM_Pre { get; set; }
        public string BPD_Post { get; set; }
        public string BPD_Pre { get; set; }
        public string Mandays_Post { get; set; }
        public string Mandays_Pre { get; set; }
        public string comment { get; set; }  

    }
   
}
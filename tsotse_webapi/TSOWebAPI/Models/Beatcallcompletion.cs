﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class Beatcallcompletion
    {
        public string retailercount { get; set; }
        public string beatid { get; set;}
        public string beatname { get; set; }
        public string visitdate { get; set; }
        public string detailedreason { get; set; }
        public string reason { get; set; }
    }
}
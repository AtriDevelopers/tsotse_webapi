﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class kpidsr
    {
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string ddname { get; set; }
        public string ddcode { get; set; }
        public string aa_kicker { get; set; }
        public string qsip_kicker { get; set; }
        public string index_bpm { get; set; }
        public string index_youth { get; set; }
        public string mandays { get; set; }
        public string bpd { get; set; }
        public string EC { get; set; }


    }
}
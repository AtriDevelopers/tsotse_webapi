﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class failed
    {
        public string ddname { get; set; }
        public string DDCode { get; set; }
        public string DSRId { get; set; }
        public string DSRName { get; set; }
        public string DSRType { get; set; }
        public string BeatName { get; set; }
        public string OutletName { get; set; }
        public DateTime Failedvisitdate { get; set; }
        public string ECMonth { get; set; }
        public string FailedReason { get; set; }
    }
}
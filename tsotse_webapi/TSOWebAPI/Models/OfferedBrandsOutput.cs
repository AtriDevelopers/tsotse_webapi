﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TSOWebAPI.Models
{
    public class OfferedBrandsOutput
    {
        public List<OfferedBrands> brands = new List<OfferedBrands>();
    }
   
    public class OfferedBrands
    {
        public string imageurl { get; set; }
        public string name { get; set; }
    }
  
}
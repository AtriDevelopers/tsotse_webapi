﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class pre_postImproveSubmit
    {
        public string DDName { get; set; }
        public string DSRName { get; set; }
        public string BeatName { get; set; }
        public string Pre { get; set; }
        public string Post { get; set; }
        public string PercentageChange { get; set; }
        public string comments { get; set; }
        public IList<pre_postkpi> prepostkpi { get; set; }
    }

    public class pre_postkpi
    {
        public string pre { get; set; }
        public string post { get; set; }
    }
}
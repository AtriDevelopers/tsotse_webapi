﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class DistributorsToday
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string orderwithouttax { get; set; }
        public string orderwithtax { get; set; }
        public string actualbillingwithouttax { get; set; }
        public string actualbillingwithtax { get; set; }
        public string percentagebilled { get; set; }
        public string todaysdatetime { get; set; }
    }
}
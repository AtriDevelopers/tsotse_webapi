﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public enum Month
    {
        January,

        February,

        March,

        April,

        May,

        June,

        July,

        August,

        September,

        October,

        November,

        December

    }

    public class MonthResponse
    {
        public IList<TSOMonth> months { get; set; }
    }

    public class TSOMonth
    {
        public String name { get; set; }
        public String id { get; set; }
    }
    
}
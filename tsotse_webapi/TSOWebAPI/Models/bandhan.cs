﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class bandhan
    {
        public string Program { get; set; }
        public string Distributorcode { get; set; }
        public string DistributorName { get; set; }
        public string DSR_Name { get; set; }
        public string Beat_Name { get; set; }
        public string RetailerCode { get; set; }
        public string RetailerName { get; set; }
        public string PhaseTarget { get; set; }
        public string PhaseAchievement { get; set; }
        public string Payout { get; set; }
        public string Achieved { get; set; }

        public string MonthTarget { get; set; }
        public string MonthAchievement { get; set; }
        public string MonthPercentAchievement { get; set; }
    }
}
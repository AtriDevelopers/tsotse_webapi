﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class ResponseTest
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string dsrid { get; set; }
    }

    public class ResponseTestVM :ResponseBase
    {
        public IList<ResponseTest> data;
    }
}
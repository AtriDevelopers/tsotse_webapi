﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using TSOWebAPI.Models;

namespace TSOWebAPI
{
    public class DataBaseMain
    {
      readonly SqlConnection _sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSOConnection"].ConnectionString);
     // readonly SqlConnection _sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["localConnection"].ConnectionString);

        public void Executenonqurie(string query)
        {
            var cmd = new SqlCommand(query, _sqlConnection);
          
            if (_sqlConnection.State == ConnectionState.Closed)
                _sqlConnection.Open();

            cmd.ExecuteNonQuery();
            _sqlConnection.Close();

        }
        public DataTable Getdata(string str1)
        {
            if (_sqlConnection.State == ConnectionState.Closed)
                _sqlConnection.Open();
            var cmd = new SqlCommand(str1, _sqlConnection);
            var dt = new DataTable();
            var ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            _sqlConnection.Close();
            return dt;
        }
        public DataSet GetTable(string query)
        {
            if (_sqlConnection.State == ConnectionState.Closed)
                _sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandTimeout = 600;
            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter(query, _sqlConnection);
            ad.Fill(ds);
            _sqlConnection.Close();
            return ds;
            //return null;
        }

       

        public void pushnotifylog(string token,string result,string requestBody)
        {
            IList<FcmTokenInput> inputtso = new List<FcmTokenInput>();
            var ds = GetTable("exec spAddUser '" + token + "'");
            
        }
       
    }
    
}
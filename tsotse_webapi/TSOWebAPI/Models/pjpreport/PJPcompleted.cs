﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class Pjpcompleted
    {
        public string ddcode { get; set; }
        public string dsrid { get; set; }
        public string beatid { get; set; }
        public string beatname { get; set; }
        public string todaysachivement { get; set; }
        public IList<Kpiname> kpiname { get; set; }

       
    }
    public class Kpiname
    {
        public string nameofkpi { get; set; }
        public string todaysach { get; set; }
        //public string Averagekpi { get; set; }
    }
   
}
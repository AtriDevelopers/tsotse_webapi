﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models.pjpreport
{
    public class Beatlist
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public Boolean complete_incomplete { get; set; }
        public IList<Beat> beatkpiname { get; set; }
    }
    public class Beat
    { 
        public string beatid { get; set; }
        public string beatname { get; set; }
    }
}
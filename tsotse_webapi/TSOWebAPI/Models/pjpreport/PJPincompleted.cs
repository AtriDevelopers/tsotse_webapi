﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class Pjpincompleted
    {
        public string dsrid { get; set; }
        public string remark { get; set; }
        public string reason { get; set; }
        public string beatid { get; set; }
        public string beatname { get; set; }

        public string ddcode { get; set; }
    }
}
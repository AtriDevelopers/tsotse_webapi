﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models.pjpreport
{
    public class submitdsr
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public Boolean isSelected { get; set; }
    }
}
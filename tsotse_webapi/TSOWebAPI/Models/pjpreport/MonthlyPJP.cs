﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models.pjpreport
{
    public class MonthlyPJP 
    {
        public string ddcode { get; set; }
        public string dsrid { get; set; }
        public string ddname { get; set; }
        public string dsrname { get; set; }
        public string agenda { get; set; }
        public string pjpdate { get; set; }
    }

    public class MonthlyPJPVM : ResponseBase
    {
        public List<MonthlyPJP> pjplist;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models.pjpreport
{
    public class dsraveragekpi
    {
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public Boolean isSelected { get; set; }
        public Boolean isAttended { get; set; }
        public Boolean isTodaysselected { get; set; }
        public string Averagevalue { get; set; }
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public DateTime currentdate { get; set; }
        public string updatedate { get; set; }
        public IList<KpiPercentValue> kpilist { get; set; }
        
    }
    public class KpiPercentValue
    {
       
        public string kpiname { get; set; }
        public string percentageAch { get; set; }
        public string kpiach { get; set; }
        public string kpitarget { get; set; }
    }
}
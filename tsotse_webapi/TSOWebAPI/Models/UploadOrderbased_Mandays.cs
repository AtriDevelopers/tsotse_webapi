﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class UploadOrderbased_Mandays
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string uploadcount { get; set; }
        public string downloadcount { get; set; }
        public string dsrcount { get; set; }
        public string dsrwithMandays { get; set; }
        public string TotalPC { get; set; }
        public string PC200 { get; set; }
        public string ordervalue { get; set; }
    }

    public class UploadOrderbased_MandaysVM: ResponseBase
    {
        public List<UploadOrderbased_Mandays> data;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class Udan
    {
        public string code { get; set; }
        public string name { get; set; }
        public string earning_potential { get; set; }
        
        public string mtd_earning { get; set; }
        public string filtermonth { get; set; }



    }
    public class udancurrentmonth
    {
        public string months { get; set; }
        public IList<Udan> dds { get; set; }
    }
    
    
    public class udankpi
    {
        public string kpi { get; set; }
        public string target { get; set; }
        public string achievement { get; set; }
        public string mtd { get; set; }
        public string slabpercentage { get; set; }
        public string month { get; set; }
 
    }


 

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class BeatUploadDownload
    {

        public string DDID { get; set; }
        public string DDName { get; set; }
        public string DownloadFlag { get; set; }
        public string DownloadTime { get; set; }
        public string UploadTime { get; set; }
        public string UploadFlag { get; set; }
        public string DSRCode { get; set; }
        public string DSRName { get; set; }
        public string DSRType { get; set; }
        public string TodaysDateTime { get; set; }
        public string LastUploadedDate { get; set; }
        public string ordervalue { get; set; }
    }
}
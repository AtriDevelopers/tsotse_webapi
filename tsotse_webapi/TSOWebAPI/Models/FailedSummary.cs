﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class FailedSummary
    {
        public string ddcode { get; set; }
        public string ddname { get; set; }
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string beatid { get; set; }
        public string beatname { get; set; }
        public string outlet { get; set; }
        public string Universe { get; set; }
    }

}
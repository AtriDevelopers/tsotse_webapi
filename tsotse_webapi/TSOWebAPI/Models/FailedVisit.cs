﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class FailedVisit
    {
        public string ddname { get; set; }
        public string ddcode { get; set; }
        public string dsrid { get; set; }
        public string dsrname { get; set; }
        public string dsrtype { get; set; }
        public string beatname { get; set; }
        public string outletname { get; set; }
        public DateTime failedvisitdate { get; set; }
        public string ecmonth { get; set; }
        public string failedreason { get; set; }


    }
}
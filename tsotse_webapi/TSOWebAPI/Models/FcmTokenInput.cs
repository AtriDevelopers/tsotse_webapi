﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI.Models
{
    public class FcmTokenInput
    {
        public string to { get; set; }
        public Data data { get; set; }
        public Notification notification { get; set; }
    }
    public class Data
    {
        public string screen { get; set; }
        public string uploadcount { get; set; }
        public string downloadcount { get; set; }
        public string ordervalue { get; set; }
    }
    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string tag { get; set; }
        public string click_action { get; set; }
        public string vibrate { get; set; }
        public string screen { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class RetailIssue
    {
        public string ProblemDate { get; set; }
        public string DSRId { get; set; }
        public string DSRName { get; set; }
        public string BeatName { get; set; }
        public string RetailerName { get; set; }
        public string IssueName { get; set; }
        public string DistCode { get; set; }
        public string IssueDetailId { get; set; }
        public string RetailerCode { get; set; }
        public string Updatedbyid { get; set; }
        public string Updatedbyname { get; set; }
        public string IssueRemark { get; set; }
        public string Remark { get; set; }
        public string IssueStatus { get; set; }
        public string IssueId { get; set; }

    }





      
    
}
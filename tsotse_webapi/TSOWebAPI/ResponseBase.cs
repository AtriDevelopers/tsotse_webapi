﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace TSOWebAPI
{
    public class ResponseBase
    {
        public int StatusCode { get; set; }
        public string StatusDescription { get; set; }
        
    }
}
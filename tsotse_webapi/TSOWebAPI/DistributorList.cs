﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSOWebAPI
{
    public class DistributorList
    {
        public string DISTRIBUTOR_CODE { get; set; }
        public string DISTRIBUTOR_NAME { get; set; }
        public string TSO_TSE_CODE { get; set; }
        public string TSO_TSE_NAME { get; set; }
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TSOWebAPI
{
    public class DataBaseGTP
    {
        readonly SqlConnection _sqlConnection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["GTPConnection"].ConnectionString);

        public void Executenonquery(string query1)
        {
            var cmd = new SqlCommand(query1, _sqlConnection1);

            if (_sqlConnection1.State == ConnectionState.Closed)
                _sqlConnection1.Open();

            cmd.ExecuteNonQuery();
            _sqlConnection1.Close();
        }
        public DataTable GetData(String str)
        {
            if (_sqlConnection1.State == ConnectionState.Closed)
                _sqlConnection1.Open();
            var cmd = new SqlCommand(str, _sqlConnection1); 
            var dt = new DataTable();
            var ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            _sqlConnection1.Close();
            return dt;
        }

        public DataSet GetTable(string query)
        {
            if (_sqlConnection1.State == ConnectionState.Closed)
                _sqlConnection1.Open();
            SqlCommand cmd = new SqlCommand(query);
            DataSet dt = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter(query,_sqlConnection1);
            ad.Fill(dt);
            _sqlConnection1.Close();
            return dt;
        }
    }
}
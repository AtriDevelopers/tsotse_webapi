﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class PollsSurveyReportController : ApiController
    {
       
        private readonly DataBaseMain _con = new DataBaseMain();
        [ActionName("pollssurvey")]
        [HttpGet]

        public List<PollsSurvey> Get()//it wiil give the list of polls and survey
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<PollsSurvey> polls = new List<PollsSurvey>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        //Dictionary<string, string> querystrings = Request.GetQueryNameValuePairs()
                        //  .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                        //string value;
                        //if(querystrings.TryGetValue("DistCode",out value))
                        //{

                        //}
                        if (id.ToLower() != "dummy")
                        {
                            polls = pollsSurvey(id);
                        }
                        else
                        {
                            polls = PollsSurveyDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return polls;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
               
            }
        }

        private List<PollsSurvey> pollsSurvey (string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            List<PollsSurvey> polls = new List<PollsSurvey>();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                     _con.GetTable("exec GetPollSurveyDetails '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ps = new PollsSurvey()
                        {
                            DDCode = ds.Tables[0].Rows[i]["DistributorId"].ToString(),
                            DDName = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString(),
                            DSRId = ds.Tables[0].Rows[i]["UserID"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["UserName"].ToString(),
                            ActivityName = ds.Tables[0].Rows[i]["ActivityName"].ToString(),
                            Status = ds.Tables[0].Rows[i]["Status"].ToString(),
                            CompletionDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["SurveyDate"]).ToShortDateString(),

                        };
                       
                        polls.Add(ps);
                    }

                }
                else { throw new HttpResponseException(HttpStatusCode.BadRequest); }
            }
            return polls;
        }
        private List<PollsSurvey> PollsSurveyDummy()
        {
            List<PollsSurvey> polls = new List<PollsSurvey>();
            PollsSurvey polls1 = new PollsSurvey();
            PollsSurvey polls2 = new PollsSurvey();
            PollsSurvey polls3 = new PollsSurvey();
            PollsSurvey polls4 = new PollsSurvey();
            PollsSurvey polls5 = new PollsSurvey();
           

            polls.Add(polls1);
            polls.Add(polls2);
            polls.Add(polls3);
            polls.Add(polls4);
            polls.Add(polls5);
           


           foreach (PollsSurvey PPSS in polls)
            {
               
                PPSS.DSRId = "1454";
                PPSS.DSRName = "SAFOLA";
                PPSS.ActivityName = "act1";
                PPSS.Status = "good";
                //PPSS.CompletionDate = "3-4-2017";
            }
            return polls;
        }

    }
    
}
           
                    
 
         

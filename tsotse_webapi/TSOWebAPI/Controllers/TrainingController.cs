﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class TrainingController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        
        [HttpGet]
        [ActionName("training")]
        public List<PollsSurvey> Get()
        {
            try
            {
                string id = "";

                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<PollsSurvey> polls = new List<PollsSurvey>();
                    if (!string.IsNullOrEmpty(id))
                    {

                        Dictionary<string, string> queryStrings;
                        if (id.ToLower() != "dummy")
                        {
                            queryStrings = Request.GetQueryNameValuePairs()
                       .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                            String value;
                            if (queryStrings.TryGetValue("Trainlist", out value))
                            {

                            }
                            polls = pollsSurvey(id, value);

                        }
                        else
                        {
                            polls = PollsSurveyDummy();
                        }
                    }
                   
                    return polls;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                  
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);

            }
        }
        private List<PollsSurvey> pollsSurvey(string TSO_TSE_CODE, string Trainlist)// Training details for particular TSO
        {
            string id = TSO_TSE_CODE;
            List<PollsSurvey> polls = new List<PollsSurvey>();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                     _con.GetTable("exec GetTrainingDetails '" + id + "','" + Trainlist + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ps = new PollsSurvey()
                        {
                            DDCode = ds.Tables[0].Rows[i]["DDCode"].ToString(),
                            DDName = ds.Tables[0].Rows[i]["DDName"].ToString(),
                            DSRId = ds.Tables[0].Rows[i]["dsrid"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            //ActivityName = ds.Tables[0].Rows[i]["ActivityName"].ToString(),
                            Status = ds.Tables[0].Rows[i]["Status"].ToString(),
                            //  CompletionDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["CompletionDate"].ToString()).ToShortDateString(),
                            CompletionDate = ds.Tables[0].Rows[i]["CompletionDate"].ToString(),
                        };
                        polls.Add(ps);
                    }
                }
             

            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
               
            }
            return polls;
        }
        private List<PollsSurvey> PollsSurveyDummy()
        {
            List<PollsSurvey> polls = new List<PollsSurvey>();
            PollsSurvey polls1 = new PollsSurvey();
            PollsSurvey polls2 = new PollsSurvey();
            PollsSurvey polls3 = new PollsSurvey();
            PollsSurvey polls4 = new PollsSurvey();
            PollsSurvey polls5 = new PollsSurvey();


            polls.Add(polls1);
            polls.Add(polls2);
            polls.Add(polls3);
            polls.Add(polls4);
            polls.Add(polls5);


            foreach (PollsSurvey PPSS in polls)
            {

                PPSS.DSRId = "1454";
                PPSS.DSRName = "SAFOLA";
                PPSS.ActivityName = "act1";
                PPSS.Status = "good";
                // PPSS.CompletionDate = "3-4-2017";
            }
            return polls;
        }

        [HttpGet]
        [ActionName("traininglist")]
        public IList<Traininglist> Getraining()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            string token = headers.GetValues("auth").First();
            id = token;
            List<Traininglist> traininglist = new List<Traininglist>();


            var ds = _con.GetTable("exec GetTrainingList");

            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var trainlist = new Traininglist()
                    {
                        Trainlist = ds.Tables[0].Rows[i]["TrainingTitle"].ToString(),
                    };
                    traininglist.Add(trainlist);
                }
            }



            return traininglist;
        }

        [HttpGet]
        [ActionName("traininglistnew")]
        public IList<Traininglist> Getrain()   // training list for particulae TSO 
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            string token = headers.GetValues("auth").First();
            id = token;
            List<Traininglist> traininglist = new List<Traininglist>();


            var ds = _con.GetTable("exec GetTrainingListnew '" + id + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var trainlist = new Traininglist()
                    {
                        Trainlist = ds.Tables[0].Rows[i]["TrainingTitle"].ToString(),
                    };
                    traininglist.Add(trainlist);
                }
            }



            return traininglist;
        }
    } 
}

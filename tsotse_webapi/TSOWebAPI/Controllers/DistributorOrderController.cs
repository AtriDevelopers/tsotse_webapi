﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class DistributorOrderController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [ActionName("today")]
        [HttpGet]
        public List<DistributorsToday> Get()
        {

            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<DistributorsToday> distom = new List<DistributorsToday>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            distom = DistributorsToday(id, "0");
                        }
                        else
                        {
                            distom = DistributorsTodayDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return distom;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
              
               var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }

        private List<DistributorsToday> DistributorsToday(string TSO_TSE_CODE, string durationFlag)
        {
            string id = TSO_TSE_CODE;
            List<DistributorsToday> DistributorsToday = new List<DistributorsToday>();
            if (!string.IsNullOrEmpty(id))
            {
                
                    var ds =
                        _con.GetTable("exec GetDistributorOrder '" + id + "', '" + durationFlag + "'"); 
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            var dst = new DistributorsToday()
                            {
                                ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString(),
                                ddname = ds.Tables[0].Rows[i]["ddname"].ToString(),
                                orderwithouttax = ds.Tables[0].Rows[i]["orderwithouttax"].ToString(),
                                orderwithtax = ds.Tables[0].Rows[i]["orderwithtax"].ToString(),
                                actualbillingwithouttax = ds.Tables[0].Rows[i]["actualbillingwithouttax"].ToString(),
                                actualbillingwithtax = ds.Tables[0].Rows[i]["actualbillingwithtax"].ToString(),
                                percentagebilled = ds.Tables[0].Rows[i]["percentagebilled"].ToString(),
                                todaysdatetime = Convert.ToDateTime(ds.Tables[0].Rows[i]["todaysdatetime"].ToString()).ToShortDateString(),

                            };
                            DistributorsToday.Add(dst);
                        }
                    }
                   
                }


            
            return DistributorsToday;
        }

        private List<DistributorsToday> DistributorsTodayDummy()
        {
            List<DistributorsToday> dst = new List<DistributorsToday>();
            DistributorsToday dst1 = new DistributorsToday();
            DistributorsToday dst2 = new DistributorsToday();
            DistributorsToday dst3 = new DistributorsToday();
            DistributorsToday dst4 = new DistributorsToday();
            DistributorsToday dst5 = new DistributorsToday();
            DistributorsToday dst6 = new DistributorsToday();
            DistributorsToday dst7 = new DistributorsToday();
            DistributorsToday dst8 = new DistributorsToday();

            dst.Add(dst1);
            dst.Add(dst2);
            dst.Add(dst3);
            dst.Add(dst4);
            dst.Add(dst5);
            dst.Add(dst6);
            dst.Add(dst7);
            dst.Add(dst8);

            foreach (DistributorsToday dss in dst)
            {
                dss.ddcode = "1121";
                dss.ddname = "eue";
                dss.orderwithouttax = "455.55";
                dss.orderwithtax = "4555.99";
                dss.actualbillingwithouttax = "87268.998";
                dss.actualbillingwithtax = "344.43";
                dss.percentagebilled = "55.67";
                dss.todaysdatetime= "2017-09-08";

            }
            return dst;
        }

        [ActionName("mtd")]
        //api/DistributorOrder//mtd
        [HttpGet]
        public IList<DistributorsToday> Getmtd()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<DistributorsToday> distom = new List<DistributorsToday>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            distom = DistributorsToday(id, "1");
                        }
                        else
                        {
                            distom = DistributorsTodayDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return distom;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {

                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }

     
           
    }
}
    
          



                          

  
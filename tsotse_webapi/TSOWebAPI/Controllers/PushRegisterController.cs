﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class PushRegisterController : ApiController
    {
        private DataBaseMain _con = new DataBaseMain();
        [HttpPut]
        [ActionName("registerfcmtoken")]
        public void Getregister([FromBody]pushregister register)
        {

            string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
               
                if (register != null)
                {
                    string fcm_token = register.fcm_token;
                    string deviceId = register.deviceId;
                    var ds = _con.GetTable("exec PushRegisterTSO '"+ id +"','"+ fcm_token +"','"+ deviceId +"'");
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
        }

    }
}

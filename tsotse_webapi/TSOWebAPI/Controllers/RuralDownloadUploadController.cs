﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class RuralDownloadUploadController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        [ActionName("ruraldownloaduploadcount")]
        [HttpGet]

        public BeatDownloadUploadCount Get()
        {
            BeatDownloadUploadCount beatDownloadUploadCount = new BeatDownloadUploadCount();

            string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                beatDownloadUploadCount = GetBeatDownloadUploadCount(id);
                
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return beatDownloadUploadCount;
        }

        private BeatDownloadUploadCount GetBeatDownloadUploadCount(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            BeatDownloadUploadCount BeatDownloadUploadCount = new BeatDownloadUploadCount();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                    _con.GetTable("exec RuralDownloadUploadCount '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BeatDownloadUploadCount.totaldownloadcount = ds.Tables[0].Rows[0]["totaldownloadcount"].ToString();
                    BeatDownloadUploadCount.actualdownloadcount = ds.Tables[0].Rows[0]["actualdownloadcount"].ToString();
                    BeatDownloadUploadCount.totaluploadcount = ds.Tables[0].Rows[0]["totaldownloadcount"].ToString();
                    BeatDownloadUploadCount.actualuploadcount = ds.Tables[0].Rows[0]["actualuploadcount"].ToString();
                    BeatDownloadUploadCount.todaysdatetime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE).ToString();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadGateway);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return BeatDownloadUploadCount;
        }



        [HttpGet]
        [ActionName("ruraldownoladuploadreport")]
        public List<BeatUploadDownload> GetBeatDownloadUploadRural()
        {

            string id = "";
            var re = Request;
            var headers = re.Headers;
            string token = headers.GetValues("auth").First();
            id = token;

            List<BeatUploadDownload> BeatDownloadUpload = new List<BeatUploadDownload>();
            if (!string.IsNullOrEmpty(id))
            {

                var ds =
                    _con.GetTable("exec RuralDownloadUploadDetails '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ps = new BeatUploadDownload()
                        {
                            DDID = ds.Tables[0].Rows[i]["sdcode"].ToString(),
                            DDName = ds.Tables[0].Rows[i]["distributor_name"].ToString(),
                            DownloadTime = ds.Tables[0].Rows[i]["Download"].ToString(),
                            UploadTime = ds.Tables[0].Rows[i]["Upload"].ToString(),

                           
                            DSRCode = ds.Tables[0].Rows[i]["userid"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["username"].ToString(),
                           // DSRType = ds.Tables[0].Rows[i]["DSRType"].ToString(),
                            LastUploadedDate = ds.Tables[0].Rows[i]["lastuploaddate"].ToString(),


                        };
                        BeatDownloadUpload.Add(ps);
                    }
                }
                return BeatDownloadUpload;

            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class Pre_PostController : ApiController
    {
        private DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        [ActionName("dsrpre_post")]
        public List<DSRpre_post> Getpre_post(string ddname)
        {
            List<DSRpre_post> dsrpre_post = new List<DSRpre_post>();
            var ds = _con.GetTable("exec GetDsrPre_post '" + ddname +"'");
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new DSRpre_post()
                    {
                        dsrname = ds.Tables[0].Rows[i]["Salesman_Name"].ToString()
                    };

                    dsrpre_post.Add(lt);

                }
            }
            return dsrpre_post;
        }


        [HttpGet]
        [ActionName("beatkpipre_post")]
        public List<BeatkpiPre_post> Getbeatkpipre_post(string ddname,string dsrname)
        {
            List<BeatkpiPre_post> beatpre_post = new List<BeatkpiPre_post>();
            var ds = _con.GetTable("exec GetBeatkpiPre_post '" + ddname +"','"+ dsrname +"'");
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new BeatkpiPre_post()
                    {
                        BeatName = ds.Tables[0].Rows[i]["Beat_Name"].ToString(),
                        MILEC_Post = ds.Tables[0].Rows[i]["MILEC_Post"].ToString(),
                        MILEC_Pre = ds.Tables[0].Rows[i]["MILEC_Pre"].ToString(),
                        BPM_Post = ds.Tables[0].Rows[i]["BPM_Post"].ToString(),
                        BPM_Pre = ds.Tables[0].Rows[i]["BPM_Pre"].ToString(),
                        BPD_Post = ds.Tables[0].Rows[i]["BPD_Post"].ToString(),
                        BPD_Pre = ds.Tables[0].Rows[i]["BPD_Pre"].ToString(),
                        Mandays_Post = ds.Tables[0].Rows[i]["Mandays_Post"].ToString(),
                        Mandays_Pre = ds.Tables[0].Rows[i]["Mandays_Pre"].ToString(),
                        comment = ds.Tables[0].Rows[i]["TSOComment"].ToString(),
                    };

                    beatpre_post.Add(lt);

                }
            }
            return beatpre_post;
        }
        [HttpPut]
        [ActionName("pre_postimprove")]
        public void Pjpcompleted([FromBody]pre_postImproveSubmit complete)
        {
            var re = Request;
            var headers = re.Headers;
            string id = "";
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                if (complete != null)
                {

                    pre_postkpi listKpiName = new pre_postkpi();
                    string ddname = complete.DDName;
                    string dsrname = complete.DSRName;
                    string beatname = complete.BeatName;
                    string pre = complete.Pre;
                    string post = complete.Post;
                    string PercentageChange = complete.PercentageChange;
                    string comments = complete.comments;
                    var listofkpi = complete.prepostkpi;

                    foreach (var item in listofkpi)
                    {
                        var ds =
                                 _con.GetTable("exec spDSRPrePostImprovementTrn '" + id +"','"+ ddname +"','"+ dsrname +"','"+ beatname +"','"+ item.post +"','"+ item.pre +"','"+ PercentageChange +"','"+ comments +"'");
                    }

                }
                else
                {
                    
                }
            }
            else
            {
                //if complete object is null we consider that is bad request to execute
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }



    }
}

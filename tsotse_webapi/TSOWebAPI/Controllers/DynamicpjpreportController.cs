﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Data.SqlClient;
using TSOWebAPI.Models.pjpreport;
using System.Data;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class DynamicpjpreportController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
       
        
        [HttpGet]
        [ActionName("pjpreport")]

        public List<PJPreport> pjpreport()// this api get top 8 dsr for dynamic pjp
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            string token = headers.GetValues("auth").First();
            id = token;
            List<PJPreport> pjpreport = new List<PJPreport>();
            ResponseBase _response = new ResponseBase();

            ValidateTSO objValidateToken = new ValidateTSO();
            
            if (objValidateToken.ValidateToken(token).isValid)
            {
                var ds = _con.GetTable("exec GetDSRWeeklydata  '" + id + "'");
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var lt = new PJPreport()
                        {
                            ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString(),
                            ddname = ds.Tables[0].Rows[i]["ddname"].ToString(),
                            dsrid = ds.Tables[0].Rows[i]["dsrid"].ToString(),
                            dsrname = ds.Tables[0].Rows[i]["dsrname"].ToString(),
                            averagevalue = ds.Tables[0].Rows[i]["AverageKPI"].ToString(),
                            isPJPSelected = Convert.ToBoolean(ds.Tables[0].Rows[i]["isPJPSelected"]),// this flag use for top 8 dsr validation for one week
                            isSelected = Convert.ToBoolean(ds.Tables[0].Rows[i]["isSelected"]),//this flag shows dsr selected or not
                        };

                        pjpreport.Add(lt);
                    }
                }
                return pjpreport;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadGateway);
            }


        }



        [HttpPut]
        [ActionName("completed")]
        public void Pjpcompleted([FromBody]Pjpcompleted complete)
        // submit 1 dsr into pjpattendance table with achievment from day end update 
        {
            var re = Request;
            var headers = re.Headers;
            string id = "";
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                if (complete != null)
                {

                    Kpiname listKpiName = new Kpiname();
                    string ddcode = complete.ddcode;
                    string dsrid = complete.dsrid;
                    string beatid = complete.beatid;
                    string beatname = complete.beatname;
                    var listofKpi = complete.kpiname;

                    foreach (var item in listofKpi)
                    {
                        //kpidsrr dsrkp = new kpidsrr();

                        //dsrkp.dsrid = complete.dsrid;
                        //dsrkp.nameofkpi = item.nameofkpi;
                        //dsrkp.kpivalue = item.kpivalue;

                        var ds =
                                 _con.GetTable("exec spUpdatePJPtracker '" + id + "','" + ddcode + "','" + dsrid + "','" + beatid + "','" + beatname + "','" + item.nameofkpi + "','" + item.todaysach + "'");
                    }
                }
            }
            else
            {
                //if complete object is null we consider that is bad request to execute
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpPut]
        [ActionName("incomplete")]
        public void Pjpincompleted([FromBody]Pjpincompleted incomplete)
        //submit 1 dsr with incomplete  reason
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;


            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                if (incomplete != null)
                {


                    string ddcode = incomplete.ddcode;
                    string dsrid = incomplete.dsrid;
                    string remark = incomplete.remark;
                    string reason = incomplete.reason;
                    string beatid = incomplete.beatid;
                    string beatname = incomplete.beatname;

                    var ds =
                           _con.GetTable("exec spUpdatePJPtrackerIncomplete  '" + id + "','" + ddcode + "','" + dsrid + "','" + remark + "','" + reason + "','" + beatid + "','" + beatname + "'");

                }
            }
            else
            {
                //if incomplete object is null we consider that is the bad request to execute
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }

        [HttpGet]
        [ActionName("incompletereason")]
        public List<Incomplete> Getincomplete()// get incomplete reson if dsr not attend the pjp
        {
            List<Incomplete> incomplete = new List<Incomplete>();
            var ds = _con.GetTable("exec GetIncompleteReason");
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new Incomplete()
                    {
                        failedvisit_reason = ds.Tables[0].Rows[i]["failedvisit_reason"].ToString()
                    };

                    incomplete.Add(lt);

                }
            }
            return incomplete;
        }


        [HttpPut]
        [ActionName("submitdsr")]

        public void submitdsr([FromBody]IList<submitdsr> select)
        // submit 5 dsr in db from selected 8 dsr (pjpreport screen)
        {
            var re = Request;
            var headers = re.Headers;

            string id = "";
            IList<submitdsr> firstFive = select.Take(5).ToList();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;


                foreach (var item in firstFive)
                {

                    string ddcode = item.ddcode;
                    string ddname = item.ddname;
                    string dsrid = item.dsrid;
                    string dsrname = item.dsrname;
                    item.isSelected = true; // this flag for submit 5 dsr set the flag is 1
                    //string isSelected = item.isSelected(true);
                    var ds =
                           _con.GetTable("exec PJPSubmitDSR  '" + id + "','" + ddcode + "','" + ddname + "','" + dsrid + "','" + dsrname + "','" + item.isSelected + "'");

                }

            }
        }

        [HttpPut]
        [ActionName("pjpsubmit1dsr")]
        public void pjpsubmit1dsr([FromBody]selectdsr submit1dsr)// submit 1 dsr from selected 5 dsr 
        {
            var re = Request;
            var headers = re.Headers;
            string id = "";
           
            string token = headers.GetValues("auth").First();
            id = token;
            if (submit1dsr != null)
            {

                string ddcode = submit1dsr.ddcode;
                string dsrid = submit1dsr.dsrid;
                string dsrname = submit1dsr.dsrname;
                string isSelected = submit1dsr.isSelected ? "1" : "0";// set the flag is to be 1
                string isAttended = submit1dsr.isAttended ? "1" : "0";// flag to be set if dsr go with tso,flag = 1

                var ds =
                       _con.GetTable("exec Submit1DSR '" + id + "', '" + ddcode + "','" + dsrid + "','" + dsrname + "','" + isSelected + "','" + isAttended + "'");

            }

            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpGet]
        [ActionName("dsraveragekpi")]
        public IList<dsraveragekpi> Getdsrkpiaverage()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            IList<dsraveragekpi> achievementdsr = new List<dsraveragekpi>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                if (string.IsNullOrEmpty(id))
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                else
                {
                    var ds = _con.Getdata("exec PJP5dsrSelection '" + id + "'");

                    if (ds.Rows.Count > 0)
                    {

                        var kpi = (from DataRow dRow in ds.Rows
                                   select new
                                   {
                                       dsrid = dRow["DSRId"].ToString(),
                                       dsrname = dRow["DSRName"].ToString(),
                                       ddcode = dRow["ddcode"].ToString(),
                                       ddname = dRow["ddname"].ToString(),
                                       isSelected = Convert.ToBoolean(dRow["isSelected"]),
                                       isAttended = Convert.ToBoolean(dRow["isAttended"]),
                                       Averagevalue = dRow["Averagekpi"].ToString(),
                                       kpiname = dRow["kpiName"].ToString(),
                                       percentageAch = dRow["kpiValue"].ToString(),
                                       kpiach = dRow["kpiAch"].ToString(),
                                       kpitarget = dRow["kpiTarget"].ToString(),
                                       updatedate = Convert.ToDateTime(dRow["Modifieddate"]).ToString("yyyy-MM-dd"),
                                       currentdate = DateTime.Today,
                                       isTodaysselected = Convert.ToDateTime(dRow["Modifieddate"]).ToString("yyyy-MM-dd") == (DateTime.Today).ToString("yyyy-MM-dd") ? true : false,
                                   }).ToList().Distinct();


                        var result = kpi.GroupBy(a => new { a.ddcode, a.dsrid });

                        foreach (var group in result)
                        {

                            dsraveragekpi dsrpercentage = new dsraveragekpi();
                            dsrpercentage.ddcode = group.Key.ddcode;//Select(x => x.ddcode).ToString();
                            dsrpercentage.dsrid = group.Key.dsrid;//Select(x => x.dsrid).ToString();

                            IList<KpiPercentValue> percentvalue = new List<KpiPercentValue>();

                            foreach (var value in group)
                            {

                                dsrpercentage.dsrname = value.dsrname;
                                dsrpercentage.ddname = value.ddname;
                                dsrpercentage.Averagevalue = value.Averagevalue;
                                dsrpercentage.isSelected = value.isSelected;
                                dsrpercentage.isAttended = value.isAttended;
                                dsrpercentage.updatedate = value.updatedate;
                                dsrpercentage.currentdate = value.currentdate;
                                dsrpercentage.isTodaysselected = value.isTodaysselected;
                                KpiPercentValue kpiavg = new KpiPercentValue();
                                kpiavg.kpiname = value.kpiname;
                                kpiavg.percentageAch = value.percentageAch;
                                kpiavg.kpiach = value.kpiach;
                                kpiavg.kpitarget = value.kpitarget;
                                percentvalue.Add(kpiavg);
                            }

                            dsrpercentage.kpilist = percentvalue;

                            achievementdsr.Add(dsrpercentage);
                        }
                    }
                    return achievementdsr;
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [ActionName("taskofday")]
        public IList<dsraveragekpi> GetTaskday()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            IList<dsraveragekpi> achievementdsr = new List<dsraveragekpi>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                if (string.IsNullOrEmpty(id))
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                else
                {
                    var ds = _con.Getdata("exec Taskoftheday '" + id + "'");

                    if (ds.Rows.Count > 0)
                    {

                        var kpi = (from DataRow dRow in ds.Rows
                                   select new
                                   {
                                       dsrid = dRow["DSRId"].ToString(),
                                       dsrname = dRow["DSRName"].ToString(),
                                       ddcode = dRow["ddcode"].ToString(),
                                       ddname = dRow["ddname"].ToString(),
                                       isSelected = Convert.ToBoolean(dRow["isSelected"]),
                                       isAttended = Convert.ToBoolean(dRow["isAttended"]),
                                       Averagevalue = dRow["Averagekpi"].ToString(),
                                       kpiname = dRow["kpiName"].ToString(),
                                       kpiach = dRow["kpiAch"].ToString(),
                                       kpitarget = dRow["kpiTarget"].ToString(),
                                       percentageAch = dRow["kpiValue"].ToString(),
                                       updatedate = Convert.ToDateTime(dRow["Modifieddate"]).ToString("yyyy-MM-dd"),
                                       currentdate = DateTime.Today,
                                       isTodaysselected = Convert.ToDateTime(dRow["Modifieddate"]).ToString("yyyy-MM-dd") == (DateTime.Today).ToString("yyyy-MM-dd") ? true : false,
                                   }).ToList().Distinct();


                        var result = kpi.GroupBy(a => new { a.ddcode, a.dsrid });

                        foreach (var group in result)
                        {
                            dsraveragekpi dsrpercentage = new dsraveragekpi();
                            dsrpercentage.ddcode = group.Key.ddcode;//Select(x => x.ddcode).ToString();
                            dsrpercentage.dsrid = group.Key.dsrid;//Select(x => x.dsrid).ToString();

                            IList<KpiPercentValue> percentvalue = new List<KpiPercentValue>();

                            foreach (var value in group)
                            {
                               
                                //dsrpercentage.ddcode = value.ddcode;
                                dsrpercentage.dsrname = value.dsrname;
                                dsrpercentage.ddname = value.ddname;
                                dsrpercentage.Averagevalue = value.Averagevalue;
                                dsrpercentage.isSelected = value.isSelected;
                                dsrpercentage.isAttended = value.isAttended;
                                dsrpercentage.updatedate = value.updatedate;
                                dsrpercentage.currentdate = value.currentdate;
                                dsrpercentage.isTodaysselected = value.isTodaysselected;
                                KpiPercentValue kpiavg = new KpiPercentValue();
                                kpiavg.kpiname = value.kpiname;
                                kpiavg.percentageAch = value.percentageAch;
                                kpiavg.kpiach = value.kpiach;
                                kpiavg.kpitarget = value.kpitarget;
                                percentvalue.Add(kpiavg);
                            }

                            dsrpercentage.kpilist = percentvalue;

                            achievementdsr.Add(dsrpercentage);
                        }
                    }
                    return achievementdsr;
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpGet]
        [ActionName("beatnamelist")]
        public List<Beatlist> Getbeat()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<Beatlist> beatlist = new List<Beatlist>();
                if (string.IsNullOrEmpty(id))
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                else
                {
                    var ds = _con.Getdata("EXEC GetBeatlist '" + id + "'");
                    if (ds.Rows.Count > 0)
                    {
                        var beat = (from DataRow dRow in ds.Rows
                                    select new
                                    {
                                        dsrid = dRow["DSRId"].ToString(),
                                        dsrname = dRow["DSRName"].ToString(),
                                        ddcode = dRow["ddcode"].ToString(),
                                        ddname = dRow["DDName"].ToString(),
                                        beatid = dRow["beat_id"].ToString(),
                                        beatname = dRow["beat_name"].ToString(),
                                        complete_incomplete = Convert.ToBoolean(dRow["complete_Incomplete"]),
                                    }).ToList().Distinct();

                        var result = beat.GroupBy(a => new { a.ddcode, a.dsrid });

                        foreach (var group in result)
                        {
                            Beatlist beatlistkpi = new Beatlist();
                            beatlistkpi.ddcode = group.Key.ddcode;//Select(x => x.ddcode).ToString();
                            beatlistkpi.dsrid = group.Key.dsrid;//Select(x => x.dsrid).ToString();
                                                                // dsrpercentage.Averagevalue= group.Select(x => x.Averagevalue).ToString();
                                                                //dsrpercentage.ddcode = group.Key;
                                                                // dsrpercentage.Averagevalue = group.Key
                                                                //dsrpercentage.isSelected = group.Key;

                            List<Beat> beatkpi = new List<Beat>();

                            foreach (var value in group)
                            {
                                ////dsrpercentage.dsrid = value.dsrid;
                                //dsrpercentage.ddcode = value.ddcode;

                                beatlistkpi.ddname = value.ddname;
                                beatlistkpi.dsrname = value.dsrname;
                                beatlistkpi.complete_incomplete = value.complete_incomplete;
                                Beat bt = new Beat();
                                bt.beatid = value.beatid;
                                bt.beatname = value.beatname;



                                beatkpi.Add(bt);


                            }
                            beatlistkpi.beatkpiname = beatkpi;


                            beatlist.Add(beatlistkpi);
                        }

                    }

                    return beatlist;

                }

            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }

        [HttpGet]
        [ActionName("dsrweight")]
        public IList<DSRWeightage> Getdsrwt()
        {
            List<DSRWeightage> weight = new List<DSRWeightage>();

            var ds = _con.GetTable("exec GetDSRKPI");
            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var dsrwt = new DSRWeightage()
                    {
                        kpiname = ds.Tables[0].Rows[i]["kpiname"].ToString(),
                        dsrweightage = ds.Tables[0].Rows[i]["KPI"].ToString(),


                    };
                    weight.Add(dsrwt);
                }
            }
            return weight;

        }

        [HttpGet]
        [ActionName("kpip3Achievement")]
        public List<kpip3Achievement> getbeat(string ddcode, string dsrid, string beatid)
        {
            string id = ddcode;
            List<kpip3Achievement> achievement = new List<kpip3Achievement>();
            if (!string.IsNullOrEmpty(id))
            {
                var ds = _con.GetTable("exec Getp3Achievement '" + id + "','" + dsrid + "','" + beatid + "'");
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var kpiach = new kpip3Achievement()
                        {
                            kpiname = ds.Tables[0].Rows[i]["KPIName"].ToString(),
                            kpivalue = ds.Tables[0].Rows[i]["Outlet"].ToString(),

                        };
                        achievement.Add(kpiach);
                    }
                }
            }
            return achievement;
        }
    }
}















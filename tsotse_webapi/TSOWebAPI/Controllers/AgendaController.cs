﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{

    public class AgendaController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [HttpGet]
        [ActionName("agendalistnew")]
        public agendalist Get()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;


            agendalist objmsglist = new agendalist();
            IList<Agenda> objmsg = new List<Agenda>();

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                Agenda _agenda = new Agenda();
                var ds = _con.GetTable("exec GetAgendalistNew '" + id + "'");
             
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                    {
                        Agenda msg1 = new Agenda();
                        msg1.agenda = row["AgendaTitle"].ToString();

                        objmsg.Add(msg1);
                    }
                 
                }
                else
                {
                    _agenda.agenda = "Agendalist is empty";
                    objmsg.Add(_agenda);
                }


                objmsglist.agendatitle = objmsg;


            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return objmsglist;
        }



        [HttpGet]
        [ActionName("agendalist")]
        public IList<agendalistold> GetAgenda()
        {
            IList<agendalistold> agendaobj = new List<agendalistold>();


            var ds = _con.GetTable("exec GetAgendalist");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var agenda = new agendalistold()
                    {
                        AgendaTilte = ds.Tables[0].Rows[i]["AgendaTitle"].ToString()
                    };
                    agendaobj.Add(agenda);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return agendaobj;
         }

    }
}

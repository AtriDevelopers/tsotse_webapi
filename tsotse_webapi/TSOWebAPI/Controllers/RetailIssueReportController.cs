﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;
using System.Timers;


namespace TSOWebAPI.Controllers
{
    public class RetailIssueReportController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]

        public List<RetailIssue> Get()
        {

            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<RetailIssue> distom = new List<RetailIssue>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        Dictionary<string, string> queryStrings;
                        if (id.ToLower() != "dummy")
                        {
                            queryStrings = Request.GetQueryNameValuePairs()
                      .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                            String value;
                            if (queryStrings.TryGetValue("DistCode", out value))
                            {

                            }


                            distom = retailIssue(id, value);
                        }
                        else
                        {
                            distom = retailIssueDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return distom;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {

                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }
        private List<RetailIssue> retailIssue(string TSO_TSE_CODE, string DistCode)
        {
            string id = TSO_TSE_CODE;
            List<RetailIssue> retailIssue = new List<RetailIssue>();
            if (!string.IsNullOrEmpty(id))
            {

                var ds =
                    _con.GetTable("exec GetRetailerIssueReportNew '" + id + "','" + DistCode + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dst = new RetailIssue()
                        {
                            ProblemDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["ProblemDate"].ToString()).ToShortDateString(),

                            DSRId = ds.Tables[0].Rows[i]["UserId"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["UserName"].ToString(),
                            BeatName = ds.Tables[0].Rows[i]["Beat_Name"].ToString(),
                            RetailerName = ds.Tables[0].Rows[i]["Retailer_name"].ToString(),
                            IssueName = ds.Tables[0].Rows[i]["IssueName"].ToString(),
                            IssueStatus = ds.Tables[0].Rows[i]["IssueStatus"].ToString(),
                            IssueRemark = ds.Tables[0].Rows[i]["IssueRemark"].ToString(),
                            RetailerCode = ds.Tables[0].Rows[i]["RetailerCode"].ToString(),

                            //IssueName	RetailerCode	UserID	DsitributorId	ProblemDate	IssueStatus	IssueRemark	Retailer_name	Beat_id	Beat_Name	UserName
                            //4635B50 7   6881    2017 - 04 - 07  Other   Not Visited RAJESH SHAW - 4635B50   12  CHAKROBERIAKD   GOBINDO MONDAL  WK
                            //
                        };
                        retailIssue.Add(dst);

                    }
                }
            }
            return retailIssue;
        }


        private List<RetailIssue> retailIssueDummy()
        {
            List<RetailIssue> dst = new List<RetailIssue>();
            RetailIssue dst1 = new RetailIssue();
            RetailIssue dst2 = new RetailIssue();
            RetailIssue dst3 = new RetailIssue();
            RetailIssue dst4 = new RetailIssue();
            RetailIssue dst5 = new RetailIssue();
            RetailIssue dst6 = new RetailIssue();
            RetailIssue dst7 = new RetailIssue();
            RetailIssue dst8 = new RetailIssue();
            RetailIssue dst9 = new RetailIssue();
            RetailIssue dst10 = new RetailIssue();



            dst.Add(dst1);
            dst.Add(dst2);
            dst.Add(dst3);
            dst.Add(dst4);
            dst.Add(dst5);
            dst.Add(dst6);
            dst.Add(dst7);
            dst.Add(dst8);
            dst.Add(dst9);
            dst.Add(dst10);



            foreach (RetailIssue rtt in dst)
            {
                rtt.ProblemDate = "31/3/2017";
                rtt.DistCode = "2767";
                rtt.IssueDetailId = "32";
                rtt.DSRId = "278";
                rtt.DSRName = "FFGF";
                rtt.BeatName = "HHS";
                rtt.RetailerName = "AAA";
                rtt.IssueName = "action";
                rtt.RetailerCode = "23/3/2017";
                rtt.IssueId = "3/3/2017";

            }
            return dst;
        }

        [HttpPut]
        [ActionName("issuedetails")]
        public void RetailIssue([FromBody]IList<RetailIssue> issue)
        {

            foreach (var retailerIssue in issue)
            {

                string DistCode = retailerIssue.DistCode;
                string Remark = retailerIssue.Remark;
                string IssueStatus = retailerIssue.IssueStatus;
                string RetailerCode = retailerIssue.RetailerCode;
                string Updatedbyid = retailerIssue.Updatedbyid;
                string Updatedbyname = retailerIssue.Updatedbyname;
                //string IssueCloseDate = retailerIssue.IssueCloseDate;
                //IssueCloseDate = System.DateTime.UtcNow.ToString();
                var ds =
                      _con.GetTable("exec UpdateRetailerIssue '" + DistCode + "', '" + Remark + "','" + IssueStatus + "','" + RetailerCode + "','" + Updatedbyid + "','" + Updatedbyname + "'");
            }
        }
        
        [HttpPut]
        [ActionName("issuedetailstest")]
        public ResponseTestVM ResponseTest([FromBody]IList<ResponseTest> objresponsetlist)
        {
            ResponseTestVM objresponse = new ResponseTestVM();

            foreach (var item in objresponsetlist)
            {
                string ddcode = item.ddcode;
                string ddname = item.ddname;
                var ds = _con.GetTable("exec Getkpilist '" + ddcode + "','" + ddname + "'");
            }
            objresponse.data = objresponsetlist;
            objresponse.StatusCode = (int)Statuscode.success;
            objresponse.StatusDescription = "success";
            return objresponse;

        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class MessageController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        [ActionName("message")]
        public msglist Get()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            

            msglist objmsglist = new msglist();
            IList<Message> objmsg = new List<Message>();

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                var ds = _con.GetTable("exec GetMessageList '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Message msg1 = new Message()
                        {
                            msg = ds.Tables[0].Rows[i]["Message"].ToString()
                        };
                        objmsg.Add(msg1);
                    }
                   
                    objmsglist.message = objmsg;
                     
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return objmsglist;
        }
    }
}

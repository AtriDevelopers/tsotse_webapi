﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class TodaysMarketOrderController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        [ActionName("todaysorderpunched")]
        public IList<TodaysMarketOrder> orderpunched()
        {
            string id = "";
            var re = Request;


            var headers = re.Headers;
            string token = headers.GetValues("auth").First();
            id = token;
            List<TodaysMarketOrder> tmorder = new List<TodaysMarketOrder>();
            ValidateTSO objValidateToken = new ValidateTSO();
            if (objValidateToken.ValidateToken(token).isValid)
            {

                var ds = _con.GetTable("exec GetTodaysOrderPunched '" + id + "'");
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var lt = new TodaysMarketOrder()
                        {
                            ddocde = ds.Tables[0].Rows[i]["Distributor_code"].ToString(),
                            ddname = ds.Tables[0].Rows[i]["distributor_name"].ToString(),
                            dsrid = ds.Tables[0].Rows[i]["UserID"].ToString(),
                            dsrname = ds.Tables[0].Rows[i]["UserName"].ToString(),
                            orderdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["OrderDate"]).ToShortDateString(),
                            ordervalue = ds.Tables[0].Rows[i]["oredervalue"].ToString()

                        };
                        tmorder.Add(lt);
                        //HttpResponseMessage response = new HttpResponseMessage();
                    }
                }
                return tmorder;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [ActionName("gettodaysorderpunched")]
        public TodaysMarketOrderVM getorderpunched()
        {
            var re = Request;
            var headers = re.Headers;

            string token = headers.GetValues("auth").First();
          
                TodaysMarketOrderVM _response = new TodaysMarketOrderVM();
                IList<TodaysMarketOrder> dsrlist = new List<TodaysMarketOrder>();

                TodaysMarketOrder lt = new TodaysMarketOrder();

                lt.ddocde = "12";
                lt.ddname = "abcd";
                lt.dsrid = "2";
                lt.dsrname = "sujata";
                lt.orderdate = "23/09/2017";
                lt.ordervalue = "454.55";

                dsrlist.Add(lt);
               _response.data = dsrlist;
               _response.StatusCode = (int)Statuscode.success;
               _response.StatusDescription = "Information successfully retrieved";
               return _response;
           
        }

        

    }
}

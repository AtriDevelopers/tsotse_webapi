﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;
namespace TSOWebAPI.Controllers
{
    public class OffersController : ApiController
    {

        private readonly DataBaseMain _con = new DataBaseMain();
        //api/v1/offered_brands
        //#region OfferedBrands
        [HttpGet]
        [ActionName("offered_brands")]
        public OfferedBrandsOutput Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    OfferedBrandsOutput offers = new OfferedBrandsOutput();


                    if (string.IsNullOrEmpty(id))
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        var ds = _con.GetTable("exec OffersDetails 'A','" + id + "' ");
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                var lt = new OfferedBrands()
                                {
                                    name = ds.Tables[0].Rows[i]["name"].ToString(),
                                    imageurl = ds.Tables[0].Rows[i]["ImageUrl"].ToString()
                                };
                                offers.brands.Add(lt);
                            }
                        }
                        return offers;
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch { throw new HttpResponseException(HttpStatusCode.BadRequest); }
        }

        //#endregion
        [HttpGet]
        [ActionName("brand_offer")]
        public OffersOutput Getoffers()
        {
            try
            {
               string id = "";
                
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    OffersOutput offers = new OffersOutput();
                    Dictionary<string, string> queryStrings;
                    queryStrings = Request.GetQueryNameValuePairs()
                      .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                    String brandid;
                    if (queryStrings.TryGetValue("brandid", out brandid))
                    {

                    }
                    //offers = OffersOutput(id, brandid);
                    if (string.IsNullOrEmpty(id))
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        var ds = _con.GetTable("exec OffersDetails 'B','" + id + "','"+brandid+"'");
                        if (ds.Tables[0].Rows.Count >= 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                var lt = new Brands()
                                {
                                    name = ds.Tables[0].Rows[i]["name"].ToString(),
                                    imageurl = ds.Tables[0].Rows[i]["ImageUrl"].ToString()
                                };

                                lt.scheme.co = ds.Tables[0].Rows[i]["CO"].ToString();
                                lt.scheme.to = ds.Tables[0].Rows[i]["TO"].ToString();
                                offers.skus.Add(lt);
                                //offers.Add(lt);
                            }
                        }
                        return offers;

                    };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch { throw new HttpResponseException(HttpStatusCode.BadRequest); }

        }

      
    }
}




       
﻿using APIDataService.DatabaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models.AdminPanelClass;

namespace TSOWebAPI.Controllers.AdminPanelAPI
{
    public class LoginController : ApiController
    {
       // private readonly DataBaseMain _con = new DataBaseMain();

        [HttpPost]
        [ActionName("login")]
        public LoginVM SubmitLogin([FromBody] APIDataService.InputClass.AdminLogin _login)
        {
            LoginVM _response = new LoginVM();
        
            LoginService _service = new LoginService(_login.userName,_login.password);

            Tuple<int, string> tuple = _service.AdminLogin(_login);

            IDictionary<int, LoginVM> dictionary = new Dictionary<int, LoginVM>();

            if (tuple.Item1 == 400)
            {
                _response.StatusCode = 400;
                _response.StatusDescription = tuple.Item2.ToString();
               
            }
            else if (tuple.Item1 == 200)
            {
                _response.StatusCode = 200;
                _response.StatusDescription = tuple.Item2.ToString();
                //Adding Data to the Custom Response
                Login data = new Login();
                data.username = _login.userName;
                data.password = _login.password;
               
                _response.data = data;
            }
            return _response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class DistributorListController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]

        public List<DistributorList> Get()
        {
            try
            {
                string id = " ";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<DistributorList> list = new List<DistributorList>();
                    if (string.IsNullOrEmpty(id))
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        var ds = _con.GetTable("exec GetDistributorList '" + id + "' ");

                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                var lt = new DistributorList()
                                {
                                    DISTRIBUTOR_CODE = ds.Tables[0].Rows[i]["DISTRIBUTOR_CODE"].ToString(),
                                    DISTRIBUTOR_NAME = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString(),
                                    TSO_TSE_CODE = ds.Tables[0].Rows[i]["TSO_TSE_CODE"].ToString(),
                                    TSO_TSE_NAME = ds.Tables[0].Rows[i]["TSO_TSE_NAME"].ToString()
                                };
                                list.Add(lt);
                            }
                        }
                        return list;
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }
    }
}

            
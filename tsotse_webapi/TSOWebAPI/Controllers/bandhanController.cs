﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class bandhanController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        public List<bandhan> Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;

                    List<bandhan> Bandhan = new List<bandhan>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            Bandhan = bandhan(id);
                        }
                        else
                        {
                            Bandhan = bandhanDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return Bandhan;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

            
                
      private List<bandhan> bandhan(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            List<bandhan> bandhan = new List<bandhan>();
            if (!string.IsNullOrEmpty(id))
            {                
                    try
                    {
                        var ds =
                            _con.GetTable("exec GetBandhanReport '" + id + "'");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                             
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                var bn = new bandhan()
                                {
                                    Program = ds.Tables[0].Rows[i]["programtype"].ToString(),
                                    Distributorcode = ds.Tables[0].Rows[i]["Distributorcode"].ToString(),
                                    DistributorName = ds.Tables[0].Rows[i]["DistributorName"].ToString(),
                                    DSR_Name = ds.Tables[0].Rows[i]["DSR_Name"].ToString(),
                                    Beat_Name = ds.Tables[0].Rows[i]["Beat_Name"].ToString(),
                                    RetailerCode = ds.Tables[0].Rows[i]["RetailerCode"].ToString(),
                                    RetailerName = ds.Tables[0].Rows[i]["RetailerName"].ToString(),
                                    PhaseTarget = ds.Tables[0].Rows[i]["TargetVolume"].ToString(),
                                    PhaseAchievement = ds.Tables[0].Rows[i]["AchievedVolume"].ToString(),
                                    Payout = ds.Tables[0].Rows[i]["TotalPayoutValue"].ToString(),
                                    Achieved = ds.Tables[0].Rows[i]["Achieved"].ToString(),
                                    MonthTarget = ds.Tables[0].Rows[i]["MonthTarget"].ToString(),
                                    MonthPercentAchievement = ds.Tables[0].Rows[i]["MonthPercentAchievement"].ToString(),
                                    MonthAchievement = ds.Tables[0].Rows[i]["MonthAchievement"].ToString(),

                                };

                                bandhan.Add(bn);
                            }
                        }
                        else { throw new HttpResponseException(HttpStatusCode.BadRequest); }
                    }
                    catch { throw new HttpResponseException(HttpStatusCode.BadRequest); }
                
            }
            return bandhan;
        }
           
        private List<bandhan> bandhanDummy()
        {

            List<bandhan> bn = new List<bandhan>();
            bandhan bn1 = new bandhan();
            bn1.Program = "Anmol";
            bandhan bn2 = new bandhan();
            bn2.Program = "1233";
            bandhan bn3 = new bandhan();
            bn3.Program = "dev";
            bandhan bn4 = new bandhan();
            bn4.Program = "dslr";
            bandhan bn5 = new bandhan();
            bn5.Program = "sale ";
            bandhan bn6 = new bandhan();
            bn6.Program = "4567";
            bandhan bn7 = new bandhan();
            bn7.Program = "raj";
            bandhan bn8 = new bandhan();
            bn8.Program = "678";
            bandhan bn9 = new bandhan();
            bn9.Program = "465";
            bandhan bn10 = new bandhan();
            bn10.Program = "987";
            bandhan bn11 = new bandhan();
            bn11.Program = "561";
            bn.Add(bn1);
            bn.Add(bn2);
            bn.Add(bn3);
            bn.Add(bn4);
            bn.Add(bn5);
            bn.Add(bn6);
            bn.Add(bn7);
            bn.Add(bn8);
            bn.Add(bn9);
            bn.Add(bn10);
            bn.Add(bn11);
            foreach (bandhan bnn in bn)
            {
                bnn.Distributorcode = "1233";
                bnn.DistributorName = "dev";
                bnn.DSR_Name = "dslr";
                bnn.Beat_Name = "sale";
                bnn.RetailerCode = "4567";
                bnn.RetailerName = "raj";
                bnn.PhaseTarget = "678";
                bnn.PhaseAchievement = "465";
                bnn.Payout = "987";
                bnn.Achieved = "561";
                bnn.MonthAchievement = "90";
                bnn.MonthPercentAchievement = "465";
                bnn.MonthTarget = "987";
                
            }
            return bn;
        }
    }
}



        
    


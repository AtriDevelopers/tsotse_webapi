﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class LogoutController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [HttpPut]
        [ActionName("logout")]
        public void getlogout([FromBody]Logout logout)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
               
                if (logout != null)
                {
                    string deviceId = logout.deviceId;
                    var ds = _con.GetTable("exec Logout '" + id + "','" + deviceId + "'");
                }
            }
            
            else
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
        }
    }
}

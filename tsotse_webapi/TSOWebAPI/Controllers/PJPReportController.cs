﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;
using TSOWebAPI.Models.pjpreport;

namespace TSOWebAPI.Controllers
{
    public class PJPReportController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [HttpGet]
        [ActionName("pjpdistributor")]
        public DistributorVM Getdistributor()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            DistributorVM distributorvm = new DistributorVM();
            List<monthdate> monthdates = new List<monthdate>();
            List<faileddistributor> distributor = new List<faileddistributor>();
            List<PJPAgenda> agenda = new List<PJPAgenda>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                String query = "exec GetDistributorFromTSOPJP '" + id + "'";
                var ds = _con.GetTable(query);
               
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        monthdate dates = new monthdate();
                        dates.date = Convert.ToDateTime(ds.Tables[0].Rows[i]["DATESOFMONTH"].ToString()).ToShortDateString();
                        //dates.date = DateTime.Parse(ds.Tables[0].Rows[i]["DATESOFMONTH"].ToString());
                        ////var formatted = dates.date.ToString("yyyy-MM-dd");
                        ////dates.date = Convert.ToDateTime(formatted).ToString("yyyy-MM-dd");
                        monthdates.Add(dates);
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        faileddistributor ddlist = new faileddistributor();

                        ddlist.ddcode = ds.Tables[1].Rows[i]["Distributor_code"].ToString();
                        ddlist.ddname = ds.Tables[1].Rows[i]["Distributor_Name"].ToString();
                        distributor.Add(ddlist);
                    }
                }
                
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                        {
                            PJPAgenda list = new PJPAgenda();

                            list.AgendaTitle = ds.Tables[2].Rows[i]["Title"].ToString();
                            agenda.Add(list);
                        }
                    }


            }
            else
            {
                distributorvm.StatusCode = (int)Statuscode.error;
                distributorvm.StatusDescription = "Internal server error";
            }
            distributorvm.datelist = monthdates;
            distributorvm.distributor = distributor;
            distributorvm.pjpagenda = agenda;
            distributorvm.StatusCode = (int)Statuscode.success;
            distributorvm.StatusDescription = "Information successfully retrieved";
            return distributorvm;
        }

        [HttpGet]
        [ActionName("pjpdsrlist")]
        public DSRVM GetDSR(string ddcode)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            DSRVM _response = new DSRVM();
            List<DSR> dsrslist = new List<DSR>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
               
                String query = "exec AllDSRList '" + id + "','"+ ddcode +"'";
                var ds = _con.GetTable(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DSR objdsr = new DSR();

                        objdsr.dsrid = ds.Tables[0].Rows[i]["userid"].ToString();
                        objdsr.dsrname = ds.Tables[0].Rows[i]["username"].ToString();
                        dsrslist.Add(objdsr);
                    }
                }
            }
            else
            {
                _response.StatusCode = (int)Statuscode.error;
                _response.StatusDescription = "Internal server error";
            }
            _response.dsrlist = dsrslist;
            _response.StatusCode = (int)Statuscode.success;
            _response.StatusDescription = "Information successfully retrieved";
            return _response;
        }

        [HttpPut]
        [ActionName("monthlypjp")]
        public MonthlyPJPVM GetMonthlyPJp([FromBody]List<MonthlyPJP> monthlypjp)
        {
            MonthlyPJPVM obj = new MonthlyPJPVM();
            string id = "";
            var re = Request;
            var headers = re.Headers;
            
            if(headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                obj.pjplist = monthlypjp;
                foreach (var item in monthlypjp)
                {
                    string ddcode = item.ddcode;
                    string ddname = item.ddname;
                    string dsrid = item.dsrid;
                    string dsrname = item.dsrname;
                    string agenda = item.agenda;
                    DateTime date = Convert.ToDateTime(item.pjpdate);
                    var ds = _con.GetTable("exec SubmitMonthlyPJP '"+ id +"','"+ item.ddcode + "','" + item.ddname + "','" + item.dsrid + "','" + item.dsrname + "', '"+ item.agenda +"','" + item.pjpdate + "'");
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            obj.StatusCode = 200;
            obj.StatusDescription = "Data add successfully";
            return obj;
        }



       
    }
}

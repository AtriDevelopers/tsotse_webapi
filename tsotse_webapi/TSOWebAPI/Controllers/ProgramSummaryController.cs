﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace TSOWebAPI.Controllers
{

    public class ProgramSummaryController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [HttpGet]
        public List<ProgramSummary> Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<ProgramSummary> programSummary = new List<ProgramSummary>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            programSummary = ProgramSummary(id);
                        }
                        else
                        {
                            programSummary = ProgramSummaryDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);

                    }

                    return programSummary;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }


        }


        private List<ProgramSummary> ProgramSummary(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            List<ProgramSummary> ProgramSummary = new List<ProgramSummary>();
            if (!string.IsNullOrEmpty(id))
            {
                
                    var ds =
                        _con.GetTable("exec GetProgramSummary '" + id + "'");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            var ps = new ProgramSummary()
                            {
                                Program = ds.Tables[0].Rows[i]["ProgramType"].ToString(),
                                NoOfParties = ds.Tables[0].Rows[i]["Parties"].ToString(),
                                Target = ds.Tables[0].Rows[i]["Target"].ToString(),
                                AchievedTarget = ds.Tables[0].Rows[i]["AchievedTarget"].ToString(),
                                NotBilledPhase = ds.Tables[0].Rows[i]["NotInPhase"].ToString(),
                                NotBilledMonth = ds.Tables[0].Rows[i]["NotInMonth"].ToString()
                            };
                            ProgramSummary.Add(ps);
                        }
                    }
              
            }

            return ProgramSummary;
        }
        private List<ProgramSummary> ProgramSummaryDummy()
        {
            List<ProgramSummary> ps = new List<ProgramSummary>();
            ProgramSummary ps1 = new ProgramSummary();
            ps1.Program = "Anmol Premium";
            ProgramSummary ps2 = new ProgramSummary();
            ps2.Program = "Rishta Self Service";
            ProgramSummary ps3 = new ProgramSummary();
            ps3.Program = "Rishta C & C";
            ProgramSummary ps4 = new ProgramSummary();
            ps4.Program = "Disha";
            ProgramSummary ps5 = new ProgramSummary();
            ps5.Program = "Bandhan";
            ps.Add(ps1);
            ps.Add(ps2);
            ps.Add(ps3);
            ps.Add(ps4);
            ps.Add(ps5);
            foreach (ProgramSummary pss in ps)
            {
                pss.Target = "356.89";
                pss.AchievedTarget = "289.00";
                pss.NoOfParties = "678900";
                pss.NotBilledMonth = "4782";
                pss.NotBilledPhase = "711";
            }
            return ps;
        }
    }
}

                

     
         

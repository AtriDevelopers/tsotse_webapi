﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using TSOWebAPI.Models;




namespace TSOWebAPI.Controllers
{
    public class fcmtokenController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]

        public IList<FcmTokenInput> fcmSendNotification()
        {
            LogService($"inside fcmsendnotification: {DateTime.Now}");
            IList<FcmTokenInput> tokenList = new List<FcmTokenInput>();
            try
            {
                LogService($"inside upload fcmsendnotification");

                Data _data = new Data();
                var ds = _con.GetTable("exec GetTSOUploadDownloadNotification");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var notificationDataList = ds.Tables[0].AsEnumerable().Select(data => new FcmTokenInput
                    {
                        data = new Data
                        {
                            uploadcount = data.Field<string>("uploadcount"),
                            downloadcount = data.Field<string>("downloadcount"),
                            ordervalue = data.Field<string>("ordervalue")
                        },
                        to = data.Field<string>("fcmtoken"),

                    }).ToList();

                    foreach (FcmTokenInput input in notificationDataList)
                    {
                        Notification note = new Notification();

                        note.title = ConfigurationManager.AppSettings["Title"].ToString();
                        note.body = $"{ConfigurationManager.AppSettings["Message"]} \nUpload Count = {_data.uploadcount}, \nDownload Count = {_data.downloadcount}, \nOrder value = {_data.ordervalue}";
                        note.tag = ConfigurationManager.AppSettings["Tag"].ToString();
                        note.click_action = ConfigurationManager.AppSettings["Click_action"].ToString();
                        note.vibrate = ConfigurationManager.AppSettings["Vibrate"].ToString();
                       // note.scrren = ConfigurationManager.AppSettings["Scrren"].ToString();

                        _data.screen = ConfigurationManager.AppSettings["Scrren"].ToString();

                        input.notification = note;
                        LogService($"The Count of Tokens " + input.to.ToString());
                        SendNotification(input);
                    }

                }
            }
            catch (Exception e)
            {
                LogService($"exception {e.Message.ToString()}");
                LogService($"exception {e.StackTrace}");
            }
            return tokenList;
        }

        private void LogService(string content)
        {
            try
            {
                FileStream fs = new FileStream("E:\\TestLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(string.Format(content, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(content);
                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                //EventLog.WriteEntry("LogService Exception " + e.Message);
            }
        }
        public string SendNotification(FcmTokenInput _inputdata)
        {
            LogService($"inside send notification: {DateTime.Now}");
            string result = "";
            FcmTokenInput _inputtoken = new FcmTokenInput();
            var fcmUrl = ConfigurationManager.AppSettings["fcmUrl"].ToString();
            var AuthorizationKey = ConfigurationManager.AppSettings["AuthorizationKey"].ToString();
            var SENDER_ID = ConfigurationManager.AppSettings["SenderID"].ToString();

            _inputtoken.to = _inputdata.to;
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(_inputdata);
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(fcmUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", AuthorizationKey));

            httpWebRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            {
                Console.WriteLine(jsonString);
                streamWriter.Write(jsonString);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                {
                    result = streamReader.ReadToEnd();
                    var Response = Newtonsoft.Json.JsonConvert.DeserializeObject(result);
                }
                ResponseLog _responselog = new ResponseLog()
                {
                    Recepient = _inputdata.to,
                    Response = result,
                    url = fcmUrl

                };
                ResultLog(_responselog);
            }


            return result;
        }

        public void ResultLog(ResponseLog _responselog)
        {
            LogService($"inside ResultLog: {DateTime.Now}");
            string uploaddownload = "UploadDownloadReport";
            var ds = _con.GetTable("exec GetTSOPushNotificationResponse '" + _responselog.Response + "','" + _responselog.Recepient + "','" + _responselog.url + "','" + uploaddownload + "'");
        }
        
    }
}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class BeatDownloadUploadCountController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        [HttpGet]
        [ActionName("beatuploaddownloadcount")]
        public BeatDownloadUploadCount Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    BeatDownloadUploadCount beatDownloadUploadCount = new BeatDownloadUploadCount();
                    if (!string.IsNullOrEmpty(id))
                    {
                        beatDownloadUploadCount = GetBeatDownloadUploadCount(id);
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return beatDownloadUploadCount;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        
        }

        private BeatDownloadUploadCount GetBeatDownloadUploadCount(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            BeatDownloadUploadCount BeatDownloadUploadCount = new BeatDownloadUploadCount();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                    _con.GetTable("exec GetBeatDownloadUploadCount '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BeatDownloadUploadCount.totaldownloadcount = ds.Tables[0].Rows[0]["totaldownloadcount"].ToString();
                    BeatDownloadUploadCount.actualdownloadcount = ds.Tables[0].Rows[0]["actualdownloadcount"].ToString();
                    BeatDownloadUploadCount.totaluploadcount = ds.Tables[0].Rows[0]["totaluploadcount"].ToString();
                    BeatDownloadUploadCount.actualuploadcount = ds.Tables[0].Rows[0]["actualuploadcount"].ToString();
                    BeatDownloadUploadCount.todaysdatetime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE).ToString();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return BeatDownloadUploadCount;
        }

        [HttpGet]
        [ActionName("summaryorderbasedmandays")]
        public UploadOrderbased_MandaysVM GetOrdermandays()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            UploadOrderbased_MandaysVM _response = new UploadOrderbased_MandaysVM();
            List<UploadOrderbased_Mandays> orederlist = new List<UploadOrderbased_Mandays>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                var ds = _con.GetTable("exec GetBeatDownloadUploadCount_OrderBased_mandays '" + id + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UploadOrderbased_Mandays objdsr = new UploadOrderbased_Mandays();

                        objdsr.ddcode = ds.Tables[0].Rows[i]["DistributorID"].ToString();
                        objdsr.ddname = ds.Tables[0].Rows[i]["Distributor_name"].ToString();
                        objdsr.dsrcount = ds.Tables[0].Rows[i]["dsrcount"].ToString();
                        objdsr.downloadcount = ds.Tables[0].Rows[i]["actualdownloadcount"].ToString();
                        objdsr.uploadcount = ds.Tables[0].Rows[i]["actualuploadcount"].ToString();
                        objdsr.dsrwithMandays = ds.Tables[0].Rows[i]["Mandays"].ToString();
                        objdsr.TotalPC = ds.Tables[0].Rows[i]["Totalpc"].ToString();
                        objdsr.PC200 = ds.Tables[0].Rows[i]["PC_>250"].ToString();
                        objdsr.ordervalue = ds.Tables[0].Rows[i]["OrderValue"].ToString();
                        orederlist.Add(objdsr);
                        
                    }
                  
                }
                else
                {
                    _response.StatusCode = 204;
                    _response.StatusDescription = "Data is not found";
                }
            }
            _response.data = orederlist;
            _response.StatusCode = 200;
            _response.StatusDescription = "Data is found";
            return _response;
        }
    }
}
    


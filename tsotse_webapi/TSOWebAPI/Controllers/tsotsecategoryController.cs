﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class tsotsecategoryController : ApiController
    {
        private DataBaseMain _con = new DataBaseMain();

        

        [HttpGet]
        [ActionName("tsotype")]// check the TSO is urban and rural or both
        public HttpResponseMessage Validatetso(string id)
        {
            HttpResponseMessage response1 = null;
            try
            {
                // string id = "";
                var re = Request;
                var response = "";

                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    if (string.IsNullOrEmpty(id))
                    {
                        //throw new HttpResponseException(HttpStatusCode.BadRequest);
                        //response1 = Request.CreateResponse(HttpStatusCode.BadRequest, new String[1]);
                        return response1;
                    }

                    else
                    {

                        var ds = _con.GetTable("exec GetTso '" + id + "'");
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            response = ds.Tables[0].Rows[i]["TsoType"].ToString();
                        }
                        

                        HttpContext.Current.Response.AppendHeader("TsoType", response);

                        response1 = Request.CreateResponse(HttpStatusCode.OK, new String[1]);
                        response1.Headers.Add("TsoType", response);
                        return response1;
                        
                    }
                    //return response1;
                }
                else
                {
                    return response1;
                }

            }
            catch (Exception e)
            {
              
                response1 = Request.CreateResponse(HttpStatusCode.InternalServerError, new String[1]);
                return response1;
            }

        }
    }
}

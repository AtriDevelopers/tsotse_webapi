﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class dsrkpiController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpPost]
        public List<dsrkpi> Get([FromBody]string TSO_TSE_CODE)
        {
            List<dsrkpi> DSRKPI = new List<dsrkpi>();
            if (!string.IsNullOrEmpty(TSO_TSE_CODE))
            {
                if (TSO_TSE_CODE.ToLower() != "dummy")
                {
                    DSRKPI = dsrkpi(TSO_TSE_CODE);
                }
                else
                {
                    DSRKPI = dsrkpiDummy();
                }
            }
            var viewmodel = new { dsrkpi = DSRKPI };
            return DSRKPI;
        }
        private List<dsrkpi> dsrkpi(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            List<dsrkpi> dsrkpi = new List<dsrkpi>();
            if (!string.IsNullOrEmpty(id))
            {
                int n;
                bool isNumeric = int.TryParse(id, out n);
                if (isNumeric == true)
                {
                    try
                    {
                        var ds =
                            _con.GetTable("exec GetDSRIncentiveN  '" + id + "'");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                var dsr = new dsrkpi()
                               {
                                   dsrid = ds.Tables[0].Rows[i]["dsrid"].ToString(),
                                   aa_kicker = ds.Tables[0].Rows[i]["aa_kiker"].ToString(),
                                   qsip_kicker = ds.Tables[0].Rows[i]["qsip_kiker"].ToString(),
                                   index_bpm = ds.Tables[0].Rows[i]["index_bpm"].ToString(),
                                   index_youth = ds.Tables[0].Rows[i]["index_youth"].ToString(),
                                   mandays = ds.Tables[0].Rows[i]["manadays"].ToString(),
                                   bpd = ds.Tables[0].Rows[i]["BPD"].ToString(),
                                   EC = ds.Tables[0].Rows[i]["EC"].ToString(),
                               };
                                dsrkpi.Add(dsr);
                            }
                        }
                    }
                    catch { }

                }

            }
            return dsrkpi;
        }
        private List<dsrkpi> dsrkpiDummy()
        {
            List<dsrkpi> dsr = new List<dsrkpi>();
            dsrkpi dsr1 = new dsrkpi();
            dsr1.dsrid = "22";
            dsrkpi dsr2 = new dsrkpi();
            dsr2.aa_kicker = "40.20";
            dsrkpi dsr3 = new dsrkpi();
            dsr3.qsip_kicker = "50.40";
            dsrkpi dsr4 = new dsrkpi();
            dsr4.index_bpm = "45.56";
            dsrkpi dsr5 = new dsrkpi();
            dsr5.index_youth = "34.43";
            dsrkpi dsr6 = new dsrkpi();
            dsr6.mandays = "45.54";
            dsrkpi dsr7 = new dsrkpi();
            dsr7.bpd = "67.76";
            dsrkpi dsr8 = new dsrkpi();
            dsr8.EC = "56.65";
            dsr.Add(dsr1);
            dsr.Add(dsr2);
            dsr.Add(dsr3);
            dsr.Add(dsr4);
            dsr.Add(dsr5);
            dsr.Add(dsr6);
            dsr.Add(dsr7);
            foreach (dsrkpi dss in dsr)
            {
                dss.dsrid = "22";
                dss.aa_kicker = "40.20";
                dss.qsip_kicker = "50.40";
                dss.index_bpm = "45.56";
                dss.index_youth = "34.43";
                dss.mandays = "45.54";
                dss.bpd = "67.76";
                dss.EC = "56.65";
            }
            return dsr;
        }
    }
}
    

              


                             
    



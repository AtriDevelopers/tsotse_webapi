﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class DistributorClaimTypeController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [ActionName("dsdsclaimtype")]
        [HttpGet]
        public List<claimtypeold> Get()
        {

            try
            {
                string id = "";

                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<claimtypeold> claim = new List<claimtypeold>();

                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            claim = claimtype(id);
                        }
                        else
                        {
                            claim = claimTypeDummy();
                        }
                    }

                    return claim;

                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                }

            }
            catch (Exception e)
            {

                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }
        private List<claimtypeold> claimtype(string TSO_TSE_CODE)
        {
            string id = TSO_TSE_CODE;
            List<claimtypeold> claimType = new List<claimtypeold>();
            if (!string.IsNullOrEmpty(id))
            {

                var ds =
                    _con.GetTable("exec GetDistributorClaimType '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dst = new claimtypeold()
                        {
                            claimtype = ds.Tables[0].Rows[i]["claimType"].ToString(),
                        };
                        claimType.Add(dst);
                    }
                }
                else { throw new HttpResponseException(HttpStatusCode.Continue); }
            }



            return claimType;

        }


        private List<claimtypeold> claimTypeDummy()
        {
            List<claimtypeold> dst = new List<claimtypeold>();
            claimtypeold dst1 = new claimtypeold();

            dst.Add(dst1);


            foreach (claimtypeold dss in dst)
            {
                dss.claimtype = "Secondary Claim";

            }
            return dst;
        }

        [HttpGet]
        [ActionName("ddsdclaimtype")]
        public ClaimType GetclaimType(string ddcode, string fromdate, string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            List<ClaimTypeList> ctypeList = new List<ClaimTypeList>();
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                ClaimType claimobj = new ClaimType();
                
                var ds = _con.GetTable("exec GetDistributorClaimTypeNew '" + id + "','" + ddcode + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

                if(ds.Tables[0].Rows.Count > 0)
                {
                    for(int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ctypEe = new ClaimTypeList()
                        {
                            claimtype = ds.Tables[0].Rows[i]["ClaimType"].ToString()

                        };
                        ctypeList.Add(ctypEe);
                    }                   
                }
                claimobj.claimtype = ctypeList;
                return claimobj;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

       
        
    }
}



        

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using TSOWebAPI.Models;
using System.Data;
using System.Globalization;

namespace TSOWebAPI.Controllers
{
    public class DistributorClaimController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();

        [ActionName("dsdsclaim")]
        [HttpGet]
        public List<Dsdsclaim> Get()
        {
            try
            {
                string id = "";
                //string claimtype = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    //string ClaimType = headers.GetValues("Secondary claim").ToString();
                    id = token;
                    List<Dsdsclaim> claim = new List<Dsdsclaim>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        Dictionary<string, string> queryStrings;
                        if (id.ToLower() != "dummy")
                        {
                            queryStrings = Request.GetQueryNameValuePairs()
                       .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                            String value;
                            if (queryStrings.TryGetValue("claimtype", out value))
                            {

                            }
                            claim = dsdsclaim(id, value);

                        }
                        else
                        {
                            queryStrings = Request.GetQueryNameValuePairs()
                                 .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);

                            claim = dsdsclaimDummy();
                        }
                    }

                    return claim;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {

                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }


        private List<Dsdsclaim> dsdsclaim(string TSO_TSE_CODE, string ClaimType)
        {
            string id = TSO_TSE_CODE;

            List<Dsdsclaim> dsdsclaim = new List<Dsdsclaim>();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                      _con.GetTable("exec GetDistributorClaimDetails  '" + id + "','" + ClaimType + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ctyp = new Dsdsclaim()
                        {
                            ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString(),
                            ddname = ds.Tables[0].Rows[i]["ddname"].ToString(),
                            claimamount = ds.Tables[0].Rows[i]["claimamount"].ToString(),
                            balancepending = ds.Tables[0].Rows[i]["balancepending"].ToString(),
                            claimmonth = ds.Tables[0].Rows[i]["ClaimMonth"].ToString(),
                            createddate = Convert.ToDateTime(ds.Tables[0].Rows[i]["createddate"].ToString()).ToShortDateString(),


                        };
                        dsdsclaim.Add(ctyp);

                    }
                }

            }
            return dsdsclaim;
        }

        private List<Dsdsclaim> dsdsclaimDummy()
        {
            List<Dsdsclaim> ctyp = new List<Dsdsclaim>();
            Dsdsclaim ctyp1 = new Dsdsclaim();
            Dsdsclaim ctyp2 = new Dsdsclaim();
            Dsdsclaim ctyp3 = new Dsdsclaim();
            Dsdsclaim ctyp4 = new Dsdsclaim();
            Dsdsclaim ctyp5 = new Dsdsclaim();
            Dsdsclaim ctyp6 = new Dsdsclaim();


            ctyp.Add(ctyp1);
            ctyp.Add(ctyp2);
            ctyp.Add(ctyp3);
            ctyp.Add(ctyp4);
            ctyp.Add(ctyp5);
            ctyp.Add(ctyp6);


            foreach (Dsdsclaim dss in ctyp)
            {
                dss.ddcode = "45";
                dss.ddname = "qw";
                dss.claimamount = "323";
                dss.balancepending = "3322";
                dss.claimmonth = "jan";
                dss.createddate = "12/4/2017";
            }
            return ctyp;
        }

        [HttpGet]
        [ActionName("ddsdclaimcurrent")]
        public List<claimTime> dsdsclaimnew()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                List<claimTime> dsdsclaim = new List<claimTime>();
                
                
                var ds =
                      _con.Getdata("exec GetDistributorClaimDetailsTime  '"+ id +"'");

                if (ds.Rows.Count > 0)
                {
                    var ctyp = (from DataRow dRow in ds.Rows
                                select new
                                {
                                    ddcode = dRow["ddcode"].ToString(),
                                    ddname = dRow["ddname"].ToString(),
                                    claimamount = dRow["claimamount"].ToString(),
                                    balancepending = dRow["balancepending"].ToString(),
                                    claimmonth = dRow["ClaimMonth"].ToString(),
                                    createddate = Convert.ToDateTime(dRow["createddate"]).ToString("yyyy-MM-dd"),
                                    //claimtype = dRow["ClaimType"].ToString(),
                                    fromdate = DateTime.Today.ToString("yyyy-MM-dd"),
                                    //CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month),
                                   todate = DateTime.Today.ToString("yyyy-MM-dd"),

                                }).ToList().Distinct();

                     var result = ctyp.GroupBy(a => new { a.fromdate, a.todate });

                    foreach (var group in result)
                    {
                        claimTime time = new claimTime();
                        time.fromdate = group.Key.fromdate;
                        time.todate = group.Key.todate;

                        IList<Dsdsclaim> claimdetail = new List<Dsdsclaim>();
                        foreach (var value in group)
                        {
                            time.fromdate = value.fromdate;
                            time.todate = value.todate;
                            Dsdsclaim details = new Dsdsclaim();
                            details.ddcode = value.ddcode;
                            details.ddname = value.ddname;
                            details.claimamount = value.claimamount;
                            details.claimmonth = value.claimmonth;
                            details.balancepending = value.balancepending;
                            details.createddate = value.createddate;
                            //details.claimtype = value.claimtype;
                            claimdetail.Add(details);
                        }
                        time.claimlist = claimdetail;

                        dsdsclaim.Add(time);
                    }
                }

                return dsdsclaim;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpGet]
        [ActionName("ddsdclaimdistributor")]
        public List<claimDetails> Getdistributor(string fromdate,string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<claimDetails> claims = new List<claimDetails>();
                var ds = _con.GetTable("exec GetDistributorClaimDistributor '" + id + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var claimlist = new claimDetails()
                        {
                            ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString(),
                            ddname = ds.Tables[0].Rows[i]["ddname"].ToString(),
                        };
                        claims.Add(claimlist);
                    }
                }
                return claims;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
       [HttpGet]
       [ActionName("ddsdclaimdetails")]
       public List<Dsdsclaim>Getdetails(string ddcode,string claimtype,string fromdate,string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            if(headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<Dsdsclaim> claimdetails = new List<Dsdsclaim>();

                var ds = _con.GetTable("exec GetDistributorClaimDetailsNew '" + id + "','"+ ddcode +"','"+ claimtype +"','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var claimlist = new Dsdsclaim()
                        {
                            ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString(),
                            ddname = ds.Tables[0].Rows[i]["ddname"].ToString(),
                            claimamount = ds.Tables[0].Rows[i]["claimamount"].ToString(),
                            balancepending = ds.Tables[0].Rows[i]["balancepending"].ToString(),
                            claimmonth = ds.Tables[0].Rows[i]["ClaimMonth"].ToString(),
                            createddate = Convert.ToDateTime(ds.Tables[0].Rows[i]["createddate"].ToString()).ToShortDateString(),
                        };
                        claimdetails.Add(claimlist);
                    }
                }
                return claimdetails;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}






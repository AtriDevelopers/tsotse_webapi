﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class CallCompletionController : ApiController
    {
        private DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        [ActionName("dsrcallcompletion")]
        public List<dsrcallcompletion>Get(string ddcode)
        {

            List<dsrcallcompletion> dsrcall = new List<dsrcallcompletion>();

            var ds = _con.GetTable("exec GetDsrListForCallCompletion '"+ ddcode +"'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new dsrcallcompletion()
                    {
                        dsrid = ds.Tables[0].Rows[i]["DSRID"].ToString(),
                        dsrname = ds.Tables[0].Rows[i]["UserName"].ToString()
                    };

                    dsrcall.Add(lt);
                }
            }

            return dsrcall;
        }

        [HttpGet]
        [ActionName("dsrcallcompletionnew")]
        public List<dsrcallcompletion> Getdsrcall(string ddcode)
        {
           


            List<dsrcallcompletion> dsrcall = new List<dsrcallcompletion>();

            var ds = _con.GetTable("exec GetDsrListForCallCompletionNew '" + ddcode + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new dsrcallcompletion()
                    {
                        dsrid = ds.Tables[0].Rows[i]["DSRID"].ToString(),
                        dsrname = ds.Tables[0].Rows[i]["UserName"].ToString()
                    };

                    dsrcall.Add(lt);
                }
            }

            return dsrcall;
        }

        [HttpGet]
        [ActionName("beatcall")]
        public List<Beatcallcompletion> Getcallcompletion(string ddcode, string dsrid)
        {
            List<Beatcallcompletion> beatpre_post = new List<Beatcallcompletion>();
            var ds = _con.GetTable("exec getCallCompletion '" + ddcode + "','" + dsrid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var lt = new Beatcallcompletion()
                    {
                        retailercount = ds.Tables[0].Rows[i]["retailercount"].ToString(),
                        beatid = ds.Tables[0].Rows[i]["beatid"].ToString(),
                        beatname = ds.Tables[0].Rows[i]["beatname"].ToString(),
                        visitdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["visitdate"].ToString()).ToShortDateString(),
                        detailedreason = ds.Tables[0].Rows[i]["detailedreason"].ToString(),
                        reason = ds.Tables[0].Rows[i]["reason"].ToString(),
                     
                    };

                    beatpre_post.Add(lt);

                }
            }
            return beatpre_post;
        }

        [HttpGet]
        [ActionName("callcompleteionnew")]
        public List<Beatcallcompletion> Getcallcompletionnew(string ddcode, string dsrid)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<Beatcallcompletion> beatpre_post = new List<Beatcallcompletion>();
                var ds = _con.GetTable("exec  '"+ id +"','" + ddcode + "','" + dsrid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var lt = new Beatcallcompletion()
                        {
                            retailercount = ds.Tables[0].Rows[i]["retailercount"].ToString(),
                            beatid = ds.Tables[0].Rows[i]["beatid"].ToString(),
                            beatname = ds.Tables[0].Rows[i]["beatname"].ToString(),
                            visitdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["visitdate"].ToString()).ToShortDateString(),
                            detailedreason = ds.Tables[0].Rows[i]["detailedreason"].ToString(),
                            reason = ds.Tables[0].Rows[i]["reason"].ToString(),

                        };

                        beatpre_post.Add(lt);

                    }
                }
                return beatpre_post;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
           
        }

    }
}

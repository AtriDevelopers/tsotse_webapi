﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class MonthController : ApiController
    {
        [HttpGet]
        public MonthResponse Getmonth()
        {
       
            TSOMonth currentMonth = new TSOMonth();
            TSOMonth previousMonth1 = new TSOMonth();
            TSOMonth previousMonth2 = new TSOMonth();

            previousMonth2.id = DateTime.Now.AddMonths(-2).Year + "-" + DateTime.Now.AddMonths(-2).ToString("MM");
            previousMonth1.id = DateTime.Now.AddMonths(-1).Year + "-" + DateTime.Now.AddMonths(-1).ToString("MM");
            currentMonth.id = DateTime.Now.Year + "-" + DateTime.Now.ToString("MM");

           

            var result = DateTime.Now.AddMonths(-1).Year + "-" + DateTime.Now.AddMonths(-1).Month.ToString("yyyy-MM");
            currentMonth.name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
            previousMonth1.name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-1).Month);
            previousMonth2.name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-2).Month);
            
            MonthResponse response = new MonthResponse { months = new List<TSOMonth>()};
            response.months.Add(currentMonth);
            response.months.Add(previousMonth1);
            response.months.Add(previousMonth2);
            
       
            return response;
            
        }
    }

}

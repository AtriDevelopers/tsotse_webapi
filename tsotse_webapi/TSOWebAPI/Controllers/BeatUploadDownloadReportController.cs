﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class BeatUploadDownloadReportController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        [HttpGet]
        public List<BeatUploadDownload> Get()
        {  
             string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<BeatUploadDownload> beatDownloadUpload = new List<BeatUploadDownload>();
                if (!string.IsNullOrEmpty(id))
                {
                    beatDownloadUpload = GetBeatDownloadUpload(id);
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                }
                
                return beatDownloadUpload;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        
       
        private List<BeatUploadDownload> GetBeatDownloadUpload(string TSO_TSE_CODE)
        {

            string id = TSO_TSE_CODE;
            List<BeatUploadDownload> BeatDownloadUpload = new List<BeatUploadDownload>();
            if (!string.IsNullOrEmpty(id))
            {

                var ds =
                    _con.GetTable("exec GetBeatDownloadUploadReportnew '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var ps = new BeatUploadDownload()
                        {
                            DDID = ds.Tables[0].Rows[i]["DDID"].ToString(),
                            DDName = ds.Tables[0].Rows[i]["DDName"].ToString(),
                            DownloadFlag = ds.Tables[0].Rows[i]["DownloadFlag"].ToString(),
                            //DownloadTime = Convert.ToDateTime(ds.Tables[0].Rows[i]["DownloadTime"].ToString()).ToShortDateString(),
                            DownloadTime = ds.Tables[0].Rows[i]["DownloadTime"].ToString(),
                            UploadTime = ds.Tables[0].Rows[i]["UploadTime"].ToString(),
                           // UploadTime = Convert.ToDateTime(ds.Tables[0].Rows[i]["UploadTime"].ToString()).ToShortDateString(),
                            UploadFlag = ds.Tables[0].Rows[i]["UploadFlag"].ToString(),
                            DSRCode = ds.Tables[0].Rows[i]["DSRCode"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            DSRType = ds.Tables[0].Rows[i]["DSRType"].ToString(),
                            TodaysDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE).ToString(),
                            LastUploadedDate = ds.Tables[0].Rows[i]["LastUploadedDate"].ToString(),
                            ordervalue = ds.Tables[0].Rows[i]["orderValue"].ToString()
                        };
                        BeatDownloadUpload.Add(ps);
                    }
                }
               
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

           return BeatDownloadUpload;
        }

        
    }
}

﻿using APIDataService.DatabaseService;
using APIDataService.InputClass;
using APIDataService.OutputClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{

    // Added By Prashant 13-Feb-2018

    public class QuestionsController : ApiController
    {
        [HttpGet]
        public IList<QuestionVM> GetQuestions()
        {
            var headers = Request.Headers;

            IList<QuestionVM> questionList = new List<QuestionVM>();

            if (headers.Contains("auth"))
            {
                string Token = headers.GetValues("auth").First();

                QuestionService questionService = new QuestionService(Token);
                questionList = questionService.GetQuestionList();
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return questionList;
        }

        [HttpPost]
        [ActionName("{id}/answer")]
        public HttpResponseMessage PostAnswer(string id,[FromBody] Answer answer)
        {
            var headers = Request.Headers;

            HttpResponseMessage response = new HttpResponseMessage();

            if (headers.Contains("auth"))
            {

                string Token = headers.GetValues("auth").First();
                QuestionService questionService = new QuestionService(Token);

                answer.questionId = id;
                questionService.PostAnswer(answer);
                response.StatusCode = HttpStatusCode.OK;
            }
            else
            {

                response.StatusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }
        }
}

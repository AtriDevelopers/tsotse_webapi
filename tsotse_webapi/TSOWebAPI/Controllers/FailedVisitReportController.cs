﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSOWebAPI.Models;

namespace TSOWebAPI.Controllers
{
    public class FailedVisitReportController : ApiController
    {

        private readonly DataBaseMain _con = new DataBaseMain();
        
        [HttpGet]
        [ActionName("summaryreport")]
        public List<FailedSummary> Getsummary(string fromdate, string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<FailedSummary> summary = new List<FailedSummary>();
                var ds = _con.GetTable("EXEC FailedVisitSummary '" + id + "','"+ Convert.ToDateTime(fromdate).ToString("yyyy - MM - dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy - MM - dd") + "'");
                    
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                          FailedSummary summaryfailed = new FailedSummary();
                        
                            summaryfailed.ddcode = ds.Tables[0].Rows[i]["DistributorId"].ToString();
                            summaryfailed.ddname = ds.Tables[0].Rows[i]["Distributor_Name"].ToString();
                            summaryfailed.dsrid = ds.Tables[0].Rows[i]["DSRID"].ToString();
                            summaryfailed.dsrname = ds.Tables[0].Rows[i]["UserName"].ToString();
                            summaryfailed.beatid = ds.Tables[0].Rows[i]["BeatID"].ToString();
                            summaryfailed.beatname = ds.Tables[0].Rows[i]["Beat_Name"].ToString();
                            summaryfailed.outlet = ds.Tables[0].Rows[i]["Outlets"].ToString();
                            summaryfailed.Universe = ds.Tables[0].Rows[i]["Universe"].ToString();
                            
                            summary.Add(summaryfailed);
                        }
                    }
                    return summary;
                
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }


        [HttpGet]
        [ActionName("summaryreportnew")]
        public List<FailedSummary> GetsummaryOption(string days)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<FailedSummary> summary = new List<FailedSummary>();
                var ds = _con.GetTable("EXEC FailedVisitSummaryCR '" + id + "', '"+ days +"'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FailedSummary summaryfailed = new FailedSummary();

                        summaryfailed.ddcode = ds.Tables[0].Rows[i]["DistributorId"].ToString();
                        summaryfailed.ddname = ds.Tables[0].Rows[i]["Distributor_Name"].ToString();
                       
                        summaryfailed.dsrname = ds.Tables[0].Rows[i]["UserName"].ToString();
                        
                        summaryfailed.beatname = ds.Tables[0].Rows[i]["Beat_Name"].ToString();
                        summaryfailed.outlet = ds.Tables[0].Rows[i]["Outlets"].ToString();
                        

                        summary.Add(summaryfailed);
                    }
                }
                return summary;

            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }

        [HttpGet]
        [ActionName("faileddistributor")]
        public List<faileddistributor> Getdistributor(string fromdate, string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<faileddistributor> distributor = new List<faileddistributor>();
                faileddistributor failedDistributor = new faileddistributor()
                {
                    ddcode = DateTime.Now.TimeOfDay.Hours +"/"+ DateTime.Now.TimeOfDay.Minutes +"/"+ DateTime.Now.TimeOfDay.Seconds + DateTime.Now.TimeOfDay.Milliseconds + "",
                    ddname ="testingName for timeout"
                };
                //distributor.Add(failedDistributor);
                String query = "exec FailedVisitDistributor '" + id + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'";
                var ds = _con.GetTable(query);
                // failedDistributor.ddname = DateTime.Now.TimeOfDay.Hours + DateTime.Now.TimeOfDay.Minutes + DateTime.Now.TimeOfDay.Seconds + DateTime.Now.TimeOfDay.Milliseconds + "";
                //failedDistributor.ddname = query;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        faileddistributor ddlist = new faileddistributor();

                        ddlist.ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString();
                        ddlist.ddname = ds.Tables[0].Rows[i]["ddname"].ToString();
                        distributor.Add(ddlist);
                    }

                }
                return distributor;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }


        }

        [HttpGet]
        [ActionName("faileddistributornew")]
        public List<faileddistributor> GetdistributorList(string days)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;
                List<faileddistributor> distributor = new List<faileddistributor>();
                
                String query = "exec FailedVisitDistributorList '" + id + "','" + days + "'";
                var ds = _con.GetTable(query);
               
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        faileddistributor ddlist = new faileddistributor();

                        ddlist.ddcode = ds.Tables[0].Rows[i]["ddcode"].ToString();
                        ddlist.ddname = ds.Tables[0].Rows[i]["ddname"].ToString();
                        distributor.Add(ddlist);
                    }
                }
                return distributor;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [ActionName("faileddsr")]
        public List<FailedSummary> GetDSR(string ddcode, string fromdate, string todate)
        {

            List<FailedSummary> dsrlist = new List<FailedSummary>();

            var ds = _con.GetTable("exec FailedVisitDSR '" + ddcode + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    FailedSummary dsr = new FailedSummary();
                    
                        dsr.dsrid = ds.Tables[0].Rows[i]["DSRID"].ToString();
                        dsr.dsrname = ds.Tables[0].Rows[i]["UserName"].ToString();

                        dsrlist.Add(dsr);
                    
          
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return dsrlist;
        }

        [HttpGet]
        [ActionName("faileddsrnew")]
        public List<FailedSummary> GetDSRList(string ddcode, string days)
        {

            List<FailedSummary> dsrlist = new List<FailedSummary>();

            var ds = _con.GetTable("exec FailedVisitDSRList '" + ddcode + "','"+ days +"'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    FailedSummary dsr = new FailedSummary();

                    dsr.dsrid = ds.Tables[0].Rows[i]["DSRID"].ToString();
                    dsr.dsrname = ds.Tables[0].Rows[i]["UserName"].ToString();

                    dsrlist.Add(dsr);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return dsrlist;
        }

        [HttpGet]
        [ActionName("failedbeat")]
        public List<FailedSummary> Getbeat(string ddcode, string dsrid, string fromdate, string todate)
        {

            List<FailedSummary> beatlist = new List<FailedSummary>();

            var ds = _con.GetTable("exec FailedVisitBeat '" + ddcode + "','" + dsrid + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    FailedSummary beat = new FailedSummary();
                    
                        beat.beatid = ds.Tables[0].Rows[i]["BeatID"].ToString();
                        beat.beatname = ds.Tables[0].Rows[i]["Beat_name"].ToString();
                    
                    beatlist.Add(beat);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return beatlist;
        }

        [HttpGet]
        [ActionName("failedbeatnew")]
        public List<FailedSummary> GetbeatList(string ddcode, string dsrid, string days)
        {

            List<FailedSummary> beatlist = new List<FailedSummary>();

            var ds = _con.GetTable("exec FailedVisitBeatList '" + ddcode + "','" + dsrid + "','" + days + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    FailedSummary beat = new FailedSummary();

                    beat.beatid = ds.Tables[0].Rows[i]["BeatID"].ToString();
                    beat.beatname = ds.Tables[0].Rows[i]["Beat_name"].ToString();

                    beatlist.Add(beat);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return beatlist;
        }

        [HttpGet]
        [ActionName("failedvisit")]
        public List<FailedVisit> Getfailed(string ddcode, string dsrid, string beatid, string fromdate, string todate)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                List<FailedVisit> beatlist = new List<FailedVisit>();

                var ds = _con.GetTable("exec [GeneralTradeProd].dbo.[SP_GETFailedVisitsNEW] '" + id + "','" + ddcode + "','" + dsrid + "','" + beatid + "','" + Convert.ToDateTime(fromdate).ToString("yyyy-MM-dd") + "','" + Convert.ToDateTime(todate).ToString("yyyy-MM-dd") + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FailedVisit beat = new FailedVisit();
                        {
                            beat.ddcode = ds.Tables[0].Rows[i]["DistributorId"].ToString();
                            beat.ddname = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString();
                            beat.dsrid = ds.Tables[0].Rows[i]["DSRId"].ToString();
                            beat.dsrname = ds.Tables[0].Rows[i]["DSR_Name"].ToString();
                            beat.dsrtype = ds.Tables[0].Rows[i]["DSR_TYPE_DESC"].ToString();
                            beat.beatname = ds.Tables[0].Rows[i]["Beat_Name"].ToString();
                            beat.outletname = ds.Tables[0].Rows[i]["Retailer_Name"].ToString();
                            beat.failedreason = ds.Tables[0].Rows[i]["FailedReason"].ToString();
                            beat.ecmonth = ds.Tables[0].Rows[i]["Billed_Respective_Month"].ToString();
                            beat.failedvisitdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["VisitDate"]);
                        }
                        beatlist.Add(beat);
                    }
                }
                return beatlist;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

        }

        [HttpGet]
        [ActionName("failedvisitdetailed")]
        public List<FailedVisit> GetfailedDetailedList(string ddcode, string dsrid, string beatid, string days)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                List<FailedVisit> beatlist = new List<FailedVisit>();

                var ds = _con.GetTable("exec [GeneralTradeProd].dbo.[SP_GETFailedVisitsNEW2] '" + id + "','" + ddcode + "','" + dsrid + "','" + beatid + "','" + days + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FailedVisit beat = new FailedVisit();
                        {
                            beat.ddcode = ds.Tables[0].Rows[i]["DistributorId"].ToString();
                            beat.ddname = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString();
                            beat.dsrid = ds.Tables[0].Rows[i]["DSRId"].ToString();
                            beat.dsrname = ds.Tables[0].Rows[i]["DSR_Name"].ToString();
                            beat.dsrtype = ds.Tables[0].Rows[i]["DSR_TYPE_DESC"].ToString();
                            beat.beatname = ds.Tables[0].Rows[i]["Beat_Name"].ToString();
                            beat.outletname = ds.Tables[0].Rows[i]["Retailer_Name"].ToString();
                            beat.failedreason = ds.Tables[0].Rows[i]["FailedReason"].ToString();
                            beat.ecmonth = ds.Tables[0].Rows[i]["Billed_Respective_Month"].ToString();
                            beat.failedvisitdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["VisitDate"]);
                        }
                        beatlist.Add(beat);
                    }
                }
                return beatlist;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

        }

    }
}












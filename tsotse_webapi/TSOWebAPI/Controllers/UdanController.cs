﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class UdanController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        [HttpGet]
        
        public List<Udan> Get()
        {

            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<Udan> uddan = new List<Udan>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            uddan = Udan(id);
                        }
                        else
                        {
                            // uddan = UdanDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return uddan;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
               
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
                
            }


        }

      


        private List<Udan> Udan(string TSO_TSE_CODE)//get target,achievement the particular tso
        {
            string id = TSO_TSE_CODE;
            List<Udan> Udan = new List<Udan>();
            if (!string.IsNullOrEmpty(id))
            {
                {
                    var ds =
                        _con.GetTable("exec GetUdaanReportNew2A '" + id + "'");
                    if (ds.Tables[0].Rows.Count >= 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            var un = new Udan()
                            {
                                code = ds.Tables[0].Rows[i]["distcode"].ToString(),
                                name = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString(),
                                mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString(),
                                earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                                

                            };
                            Udan.Add(un);
                        }
                    }
                    else { throw new HttpResponseException(HttpStatusCode.BadRequest); }
                }

               
            }
            return Udan;
        }

        [HttpGet]
        [ActionName("udankpi")]
        public IList<udankpi> getkpi(string DistCode)//get kpi, target, ach, earning potential particular distributor
        {
            try
            {
                string id = "";

                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<udankpi> kpiudaan = new List<udankpi>();
                    Dictionary<string, string> queryStrings;
                    queryStrings = Request.GetQueryNameValuePairs()
                      .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                  
                    
                    if (string.IsNullOrEmpty(id))
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        var ds = _con.GetTable("exec GetUdaanReportNew2  '" + DistCode + "'");
                        if (ds.Tables[0].Rows.Count >= 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                             
                                    var lt = new udankpi()
                                    {
                                        kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                                        target = ds.Tables[0].Rows[i]["Target"].ToString(),
                                        achievement = ds.Tables[0].Rows[i]["Achievement"].ToString(),
                                        mtd = ds.Tables[0].Rows[i]["mtd"].ToString(),
                                        slabpercentage = ds.Tables[0].Rows[i]["slabpercentage"].ToString(),
                                    };
                                
                                kpiudaan.Add(lt); 
                            }
                        }
                        return kpiudaan;
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch { throw new HttpResponseException(HttpStatusCode.BadRequest); }

        }

        [HttpGet]
        [ActionName("uddanreportnew")]
       public udancurrentmonth udancurrentmonth(string id)// get the distributor details for particular month 
        {
            string tsoid = "";
            var re = Request;
            var headers = re.Headers;
            udancurrentmonth udanmonth = new udancurrentmonth();
            string filtermonth = "";

            IList<Udan> udankpilist = new List<Udan>();

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                tsoid = token;


                var ds =
                    _con.GetTable("exec GetUdaanReportNewCR '" + tsoid + "','" + id +"'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Udan un = new Udan()
                        {
                           
                            code = ds.Tables[0].Rows[i]["distcode"].ToString(),
                            name = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString(),
                            earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                            mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString(),
                            filtermonth = ds.Tables[0].Rows[i]["filtermonth"].ToString()
                        };
                        udankpilist.Add(un);
                        filtermonth = un.filtermonth;
                    }
                }
                udanmonth.months = filtermonth;
                udanmonth.dds = udankpilist;


            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return udanmonth;
        }

        [HttpGet]
        [ActionName("udankpimonth")]  // get the kpi particular distributor with month filter 
        public List<udankpi> udankpimonth(string DistCode, string month)
        {
            List<udankpi> Udan = new List<udankpi>();


            var ds =
                _con.GetTable("exec GetUdaanReportByMonth '" + DistCode + "','" + month + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var un = new udankpi()
                    {
                        kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                        target = ds.Tables[0].Rows[i]["Target"].ToString(),
                        achievement = ds.Tables[0].Rows[i]["Achievement"].ToString(),
                        mtd = ds.Tables[0].Rows[i]["mtd"].ToString(),
                        slabpercentage = ds.Tables[0].Rows[i]["slabpercentage"].ToString(),
                        
                    };
                    Udan.Add(un);
                }
            }
            return Udan;
        }

        [HttpGet]
        [ActionName("udancurrentmonth")]  // get the current month details for particular distributor 
        public udancurrentmonth udancurrentmonths()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;
            udancurrentmonth udanmonth = new udancurrentmonth();
            IList<Udan> udankpilist = new List<Udan>();

            if (headers.Contains("auth"))
            {
                string token = headers.GetValues("auth").First();
                id = token;

                
                var ds =
                    _con.GetTable("exec GetUdaanTSOByMonth '" + id + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Udan un = new Udan()
                        {
                            //kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                            code = ds.Tables[0].Rows[i]["distcode"].ToString(),
                            name = ds.Tables[0].Rows[i]["DISTRIBUTOR_NAME"].ToString(),
                            earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                            mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString()
                        
                        };
                        udankpilist.Add(un);
                    }
                }
                udanmonth.months = DateTime.Now.Year + "-" + DateTime.Now.ToString("MM");
                udanmonth.dds = udankpilist;


            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return udanmonth;
        }






    }

}

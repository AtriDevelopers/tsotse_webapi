﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class DsrIncentiveController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
        
        [HttpGet]
        [ActionName("dsrincentive")]
        public List<DsrIncentive1> Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<DsrIncentive1> dsrincentive1 = new List<DsrIncentive1>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            dsrincentive1 = DsrKpi(id,"DistCode","DsrId");
                        }
                        else
                        {
                            dsrincentive1 = DsrIncentive1Dummy();
                        }
                    }
                    else { dsrincentive1 = DsrIncentive1Dummy(); }
                    //var viewmodel = new { DsrIncentive1 = dsrincentive1 };

                    return dsrincentive1;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
                
            }

           
        }

  
       
        private List<DsrIncentive1> DsrKpi(string TSO_TSE_CODE,string DistCode,string DsrId)
        {
            string id = TSO_TSE_CODE;
            List<DsrIncentive1> dsrKpis = new List<DsrIncentive1>();
            if (!string.IsNullOrEmpty(id))
            {
                String query = "exec GetDSRIncentive 'A', '" + id + "','"+ DistCode +"','"+ DsrId +"'";
                        var ds =
                            _con.GetTable(query);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                var ps = new DsrIncentive1()
                                {
                                    dsrid = ds.Tables[0].Rows[i]["DsrID"].ToString(),
                                    dsrname = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                                    dsrtype = ds.Tables[0].Rows[i]["DsrType"].ToString(),
                                    ddcode = ds.Tables[0].Rows[i]["DistCode"].ToString(),

                                    ddname = ds.Tables[0].Rows[i]["DDNAME"].ToString(),
                                  
                                    earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                                    mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString(),
                                   
                                    
                                };
                                dsrKpis.Add(ps);
                            }
                        }                                   

            }
            return dsrKpis;
        }
        private List<DsrIncentive1> DsrIncentive1Dummy()
        {
            List<DsrIncentive1> ps = new List<DsrIncentive1>();
            DsrIncentive1 ps1 = new DsrIncentive1();
            ps1.dsrid = "11 ";
            DsrIncentive1 ps2 = new DsrIncentive1();
            ps2.dsrname = "premium";
            DsrIncentive1 ps3 = new DsrIncentive1();
            ps3.ddcode = "2344";
            DsrIncentive1 ps4 = new DsrIncentive1();
            ps4.ddname = "intrest";
            DsrIncentive1 ps5 = new DsrIncentive1();
            ps5.dsrtype = "daily";
            DsrIncentive1 ps6 = new DsrIncentive1();
            ps6.earning_potential = "140";
            DsrIncentive1 ps7 = new DsrIncentive1();
            ps7.mtd_earning = "400";
            ps.Add(ps1);
            ps.Add(ps2);
            ps.Add(ps3);
            ps.Add(ps4);
            ps.Add(ps5);
            ps.Add(ps6);
            ps.Add(ps7);
            foreach (DsrIncentive1 pss in ps)
            {
                pss.dsrid = "11";
                pss.dsrname = "premium";
                pss.ddcode = "2344";
                pss.ddname = "intrest";
                pss.dsrtype = "daily";
                pss.earning_potential = "140";
                pss.mtd_earning = "400";
            }
            return ps;
        }

        [HttpGet]
        [ActionName("currentmonth")]
        public dsrincentivecurrentmonth DsrKpicurrent()
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            string token = headers.GetValues("auth").First();
            id = token;
            IList<DsrIncentive1> dsrKpis = new List<DsrIncentive1>();
           dsrincentivecurrentmonth cumonthlist = new dsrincentivecurrentmonth();

            if (!string.IsNullOrEmpty(id))
            {
                String query = "exec GetDSRIncentiveByMonth 'A','" + id + "','','',''";
                var ds =
                    _con.GetTable(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        DsrIncentive1 du = new DsrIncentive1()
                        {
                            dsrid = ds.Tables[0].Rows[i]["DsrID"].ToString(),
                            dsrname = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            dsrtype = ds.Tables[0].Rows[i]["DsrType"].ToString(),
                            ddcode = ds.Tables[0].Rows[i]["DistCode"].ToString(),

                            ddname = ds.Tables[0].Rows[i]["DDNAME"].ToString(),

                            earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                            mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString(),
                           

                        };
                        dsrKpis.Add(du);
                    }
                }
                cumonthlist.months = DateTime.Now.Year + "-" + DateTime.Now.ToString("MM");
                cumonthlist.dsrs = dsrKpis;

            }
            return cumonthlist;
        }

        [HttpGet]
        [ActionName("dsrwisekpi")]
        public List<dsrwisekpi> getkpi(string DistCode,string DsrId)
        {
            try
            {
                // string id = "";

                var re = Request;
                var headers = re.Headers;

                //if (headers.Contains("auth"))
                //{
                //    string token = headers.GetValues("auth").First();
                //    // id = "";
                List<dsrwisekpi> kpidsr = new List<dsrwisekpi>();
                    var ds = _con.GetTable("exec GetDSRIncentive 'B', '','" + DistCode + "','" + DsrId + "'");
                    if (ds.Tables[0].Rows.Count >= 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            var lt = new dsrwisekpi()
                            {
                                kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                                target = ds.Tables[0].Rows[i]["Target"].ToString(),
                                achievement = ds.Tables[0].Rows[i]["Achievement"].ToString(),
                                mtd = ds.Tables[0].Rows[i]["mtd"].ToString(),
                                slabpercentage = ds.Tables[0].Rows[i]["slabpercentage"].ToString(),
                            };

                            kpidsr.Add(lt);
                        }
                        return kpidsr;
                    }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                   
                }
               
            }

            catch { throw new HttpResponseException(HttpStatusCode.BadRequest); }

        }

        

        [HttpGet]
        [ActionName("kpiwisedsr")]
        public List<kpiwisedsr> Getkp()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<kpiwisedsr> dsrkp = new List<kpiwisedsr>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            dsrkp= kpiwisedsr(id, "DistCode", "DsrId");
                        }
                        else
                        {
                          
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return dsrkp;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);

            }


        }

        public List<kpiwisedsr> kpiwisedsr(string TSO_TSE_CODE,string DistCode, string DsrId)
        {
            string id = TSO_TSE_CODE;
              
            List<kpiwisedsr> kpidsr = new List<kpiwisedsr>();
            if (!string.IsNullOrEmpty(id))
            {


                var ds = _con.GetTable("exec GetDSRIncentive 'C', '" + id + "','" + DistCode + "','" + DsrId + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dsr = new kpiwisedsr()
                        {
                            kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                            DistName = ds.Tables[0].Rows[i]["Distributor_Name"].ToString(),
                            DsrName = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            kpivalue = ds.Tables[0].Rows[i]["Kpivalue"].ToString(),
                        };
                        kpidsr.Add(dsr);
                    }
                }
            }
            return kpidsr;
                    
                   
        }
      
        [HttpGet]
        [ActionName("kpiwisedsrbymonth")]
        public  List<kpiwisedsr> kpiwisedsrbymonth(string month)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            if(headers.Contains("auth"))
            { 
               string token = headers.GetValues("auth").First();
                id = token;
               List<kpiwisedsr> kpidsr = new List<kpiwisedsr>();
           

                var ds = _con.GetTable("exec  GetDSRIncentiveByMonth 'C','" + id + "','','','"+ month +"'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dsr = new kpiwisedsr()
                        {
                            kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                            DistName = ds.Tables[0].Rows[i]["Distributor_Name"].ToString(),
                            DsrName = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            kpivalue = ds.Tables[0].Rows[i]["Kpivalue"].ToString(),
                            
                        };
                        kpidsr.Add(dsr);
                    }
                }
                return kpidsr;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            

        }

        [HttpGet]
        [ActionName("dsrincentivenew")]
        public dsrincentivecurrentmonth DsrKpinew(string month)
        {
            string id = "";
            var re = Request;
            var headers = re.Headers;

            string token = headers.GetValues("auth").First();
            id = token;
            IList<DsrIncentive1> dsrKpis = new List<DsrIncentive1>();
            dsrincentivecurrentmonth cumonthlist = new dsrincentivecurrentmonth();

            if (!string.IsNullOrEmpty(id))
            {
                String query = "exec GetDSRIncentivenEW '" + id + "','"+ month +"'";
                String filterMonth = "";
                var ds =
                    _con.GetTable(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        DsrIncentive1 du = new DsrIncentive1()
                        {
                            dsrid = ds.Tables[0].Rows[i]["DsrID"].ToString(),
                            dsrname = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            dsrtype = ds.Tables[0].Rows[i]["DsrType"].ToString(),
                            ddcode = ds.Tables[0].Rows[i]["DistCode"].ToString(),

                            ddname = ds.Tables[0].Rows[i]["DDNAME"].ToString(),

                            earning_potential = ds.Tables[0].Rows[i]["EarningPotential"].ToString(),
                            mtd_earning = ds.Tables[0].Rows[i]["MTDEarning"].ToString(),
                            filtermonth = ds.Tables[0].Rows[i]["filtermonth"].ToString(),
                            
                        };
                        dsrKpis.Add(du);
                        filterMonth = du.filtermonth;
                    }
                }
                cumonthlist.months = filterMonth;
                cumonthlist.dsrs = dsrKpis;

            }
            return cumonthlist;
        }

        private dsrkpi dsrkpiDummy()
        {

            dsrkpi dss = new dsrkpi();

            dss.dsrid = "22";
            dss.aa_kicker = "40.20";
            dss.qsip_kicker = "50.40";
            dss.index_bpm = "45.56";
            dss.index_youth = "34.43";
            dss.mandays = "45.54";
            dss.bpd = "67.76";
            dss.EC = "56.65";

            return dss;
        }

       

        [HttpGet]
        [ActionName("dswisekpibymonth")]
        public List<dsrwisekpi> dswisekpibymonth(string distcode, string dsrid, string month)
        {
            
           
            List<dsrwisekpi> dsrcurrentmonth = new List<dsrwisekpi>();
          

                var ds = _con.GetTable("exec GetDSRIncentiveByMonth 'B','','" + distcode + "','" + dsrid + "','" + month + "'");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dsrkpimonth = new dsrwisekpi()
                        {
                            kpi = ds.Tables[0].Rows[i]["KPI"].ToString(),
                            target = ds.Tables[0].Rows[i]["Target"].ToString(),
                            achievement = ds.Tables[0].Rows[i]["Achievement"].ToString(),
                            mtd = ds.Tables[0].Rows[i]["mtd"].ToString(),
                            slabpercentage = ds.Tables[0].Rows[i]["slabpercentage"].ToString(),
                            
                        };
                        dsrcurrentmonth.Add(dsrkpimonth);
                    }
                }
                return dsrcurrentmonth;
            
           
        }

        
    }
}

                         
                
  

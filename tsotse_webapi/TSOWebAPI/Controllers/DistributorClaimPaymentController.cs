﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class DistributorPaymentController : ApiController
    {
        private DataBaseMain _con = new DataBaseMain();
     
       [ActionName("creditreport")]
        [HttpGet]
        public List<ClaimAmount> Get()
        {
            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<ClaimAmount> amt = new List<ClaimAmount>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id.ToLower() != "dummy")
                        {
                            Dictionary<string, string> queryStrings = Request.GetQueryNameValuePairs()
                                .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                            String value;
                            if (queryStrings.TryGetValue("DistCode", out value))
                            {

                            }
                            amt = ClaimAmount(value);
                        }

                        else
                        {
                            amt = ClaimAmountDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return amt;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }

        private List<ClaimAmount> ClaimAmount(string Distcode)
        {
          
            List<ClaimAmount> ClaimAmount = new List<ClaimAmount>();
            var ds =
                      _con.GetTable("exec GetDistributorParticularClaimDetails  '" + Distcode + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var ammt = new ClaimAmount();
                    {
                        ammt.PaymentReceived = ds.Tables[0].Rows[i]["PaymentReceived"].ToString();
                        ammt.CNNO = ds.Tables[0].Rows[i]["CNNO"].ToString();
                        ammt.Date = Convert.ToDateTime(ds.Tables[0].Rows[i]["ClaimDate"].ToString()).ToShortDateString();

                    }
                    ClaimAmount.Add(ammt);
                }
            }
            return ClaimAmount;
        }

        [HttpGet]
        [ActionName("creditreportnew")]
        public List<ClaimAmount> creditreport(string Distcode, string claimtype, string fromdate,string todate)
        {

            List<ClaimAmount> ClaimAmount = new List<ClaimAmount>();
            var ds =
                      _con.GetTable("exec GetDistributorclaimCreditReport  '" + Distcode + "','"+ claimtype +"','"+ fromdate +"','"+ todate +"'");

            if (ds.Tables[0].Rows.Count > 0)
            {
              
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var ammt = new ClaimAmount()
                    {
                        PaymentReceived = ds.Tables[0].Rows[i]["PaymentReceived"].ToString(),
                        CNNO = ds.Tables[0].Rows[i]["CNNO"].ToString(),
                        Date = Convert.ToDateTime(ds.Tables[0].Rows[i]["ClaimDate"]).ToString("yyyy-MM-dd"),

                    };
                    ClaimAmount.Add(ammt);
                }
            }
            return ClaimAmount;
        }

        private List<ClaimAmount> ClaimAmountDummy()
        {
            List<ClaimAmount> ammt = new List<ClaimAmount>();
            ClaimAmount ammt1 = new ClaimAmount();
            ClaimAmount ammt2 = new ClaimAmount();
            ClaimAmount ammt3 = new ClaimAmount();
            ammt.Add(ammt1);
            ammt.Add(ammt2);
            ammt.Add(ammt3);
            foreach (ClaimAmount cmm in ammt)
            {
                cmm.PaymentReceived = "424";
                cmm.CNNO = "423";
                cmm.Date = "12/4/2017";
            }
            return ammt;
        }
    }
}
        
                    
                


    
            
    
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TSOWebAPI.Controllers
{
    public class DSRListController : ApiController
    {
        private readonly DataBaseMain _con = new DataBaseMain();
         [ActionName("dsr")]
        [HttpGet]
        public List<DSRList> Get()
        {

            try
            {
                string id = "";
                var re = Request;
                var headers = re.Headers;

                if (headers.Contains("auth"))
                {
                    string token = headers.GetValues("auth").First();
                    id = token;
                    List<DSRList> Dlist = new List<DSRList>();
                    if (!string.IsNullOrEmpty(id))
                    {
                        Dictionary<string, string> queryStrings;
                        if (id.ToLower() != "dummy")
                        {

                            queryStrings = Request.GetQueryNameValuePairs()
                       .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                            String value;
                            if (queryStrings.TryGetValue("DistCode", out value))
                            {

                            }
                         
                            Dlist = distributorlist(id,value);
                        }
                        else
                        {
                            Dlist = distributorlistDummy();
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    return Dlist;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                var message = e.Message;
                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = message;
                response.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(response);
            }
        }

        private List<DSRList> distributorlist(string TSO_TSE_CODE,string DistCode)
        {
            string id = TSO_TSE_CODE;
           // string DSRId;
            List<DSRList> distributorlist = new List<DSRList>();
            if (!string.IsNullOrEmpty(id))
            {
                var ds =
                      _con.GetTable("exec GetDsrProfile '" + id+"','"+DistCode+"'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var list = new DSRList()
                        {
                            DSRId = ds.Tables[0].Rows[i]["DSRId"].ToString(),
                            DSRName = ds.Tables[0].Rows[i]["DSRName"].ToString(),
                            DistCode = ds.Tables[0].Rows[i]["DistCode"].ToString(),
                            DDName = ds.Tables[0].Rows[i]["DDName"].ToString(),
                            BloodGroup = ds.Tables[0].Rows[i]["BloodGroup"].ToString(),
                            DateOfJoining = ds.Tables[0].Rows[i]["DateOfJoining"].ToString(),
                            Experience = ds.Tables[0].Rows[i]["Experience"].ToString(),
                            image = ds.Tables[0].Rows[i]["image"].ToString(),
                            contact = ds.Tables[0].Rows[i]["contact"].ToString(),


                        };
                        distributorlist.Add(list);
                    }
                }
              
            }
            return distributorlist;
        }
        private List<DSRList> distributorlistDummy()
        {
            List<DSRList> list = new List<DSRList>();
            DSRList list1 = new DSRList();
            DSRList list2 = new DSRList();
            DSRList list3 = new DSRList();
            DSRList list4 = new DSRList();
            DSRList list5 = new DSRList();
            DSRList list6 = new DSRList();
            DSRList list7 = new DSRList();
            DSRList list8 = new DSRList();
            DSRList list9 = new DSRList();

            list.Add(list1);
            list.Add(list2);
            list.Add(list3);
            list.Add(list4);
            list.Add(list5);
            list.Add(list6);
            list.Add(list7);
            list.Add(list8);
            list.Add(list9);
            foreach (DSRList dlist in list)
            {
                dlist.DSRId = "4566";
                dlist.DSRName = "assa";
                dlist.DistCode = "32432";
                dlist.DDName = "da";
                dlist.BloodGroup = "a+";
                dlist.DateOfJoining = "23/3/2016";
                dlist.Experience = "good";
                dlist.image = "ss";
                dlist.contact = "457688";

            }
            return list;
        }

        
    }
}

       
   
           


  